<?php
	// ======================================== \
	// Package: Mihalism Multi Forum Host 
	// Version: 3.0.0
	// Copyright (c) 2007, 2008 Mihalism, Inc.
	// License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GNU Public License
	// ======================================== /

	unset($mfh_root_path); // <- Just to be safe
	$mfh_root_path = ".{$phpbb_root_path}";
	require_once "{$mfh_root_path}source/includes/load_forum.php";

?>
