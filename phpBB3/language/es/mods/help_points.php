<?php
/**
*
* help_points [Spanish [es]] by Jakk - http://www.mundoforeros.com & http://www.ivemfinity.com
*
* @package language
* @version $Id: help_points.php 639 2010-02-21 18:34:44Z femu $
* copyright (c) 2009 wuerzi & femu
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if ( empty($lang) || !is_array($lang) )
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » „ “ — …
//

$help = array(
	array(
		0 => '--',
		1 => 'General'
	),
	array(
		0 => 'Editar mensajes',
		1 => 'Si un usuario edita su mensaje, los puntos por el mensaje son re-calculados y sólo recibe los puntos después de las ediciones. Pero el mensaje sólo se volverá a calcular, si se establecen puntos por publicar en este foro (> 0) en el Panel de Administración(ACP) y el interruptor para su publicación se establece en ON (encendido).'
	),
	array(
		0 => 'Editar temas',
		1 => 'Si un usuario edita su tema (la primera entrada en un tema), sólo recibirá los puntos por la publicación de un nuevo tema. Así que, de hecho, el tema será completamente re-calculado.
Pero el tema sólo se volverá a calcular, si establece los puntos para nuevos temas de este foro (> 0) en el Panel de Administración(ACP) y el interruptor para nuevos temas se establece en ON (encendido).'
	),
	array(
		0 => 'Editar adjuntos',
		1 => 'Si un usuario añade un archivo adjunto o se elimina un archivo adjunto, sólo los puntos para los archivos adjuntos se calculan finalmente con los mensajes. 
Si se eliminan todos los archivos adjuntos, los puntos generales para adjuntoss se restarán también. Los adjuntos se calculan siempre y no tienen nada que ver con temas y mensajes nuevos!'
	),
	array(
		0 => 'Editar encuestas',
		1 => 'Si un usuario edita su encuesta, la encuesta será completamente re-calculada y finalmente recibe los puntos de las partes que quedan. Si elimina la encuesta, todos los puntos de encuesta se restan y el usuario recibirá los puntos ya que es un tema nuevo.
Pero el tema sólo se volverá a calcular, si establece los puntos para los temas en la configuración avanzada puntos (> 0) en el Panel de Administración(ACP) y el interruptor para nuevos temas está activado. Esto se debe al hecho de que las encuestas son siempre nuevos temas!'
	),
	array(
		0 => 'Borrar mensajes',
		1 => 'Si un usuario elimina un mensaje, los puntos obtenidos se restan de su cuenta.
Si un moderador borra un mensaje, los puntos del usuario permanecen en su cuenta. También si usted tiene la purga automática activada, los puntos del usuario permanecen.'
	),
	array(
		0 => 'bbCodes',
		1 => 'Todos los carácteres dentro de un bbcode serán contados. El BBCode en sí mismo no se contará.'
	),
	array(
		0 => 'Etiquetas code',
		1 => 'Todo dentro de los bloques de código ( [code] [/code] ) no se contarán!'
	),
	array(
		0 => 'Emoticonos',
		1 => 'Los emoticonos no se cuentan. Pero tenga en cuenta, que entre dos smilies tiene espacios en blanco, que se cuentan como un carácter!'
	),
	array(
		0 => 'Carácteres y carácteres especiales',
		1 => 'Cada carácteres se contarán, si ha habilitado el recuento de carácteres. Los carácteres son todas las letras, números, carácteres especiales e incluso espacios en blanco!'
	),
	array(
		0 => 'Citas',
		1 => 'Todo entre los BBCodes cita ([quote] / [/quote]) no será contado! Sólo el mensaje en sí y el texto fuera de la cita se cuenta.'
	),
	array(
		0 => 'Nota importante!',
		1 => 'Sólo el texto dentro de BBCodes será contado, pero no el BBCode. El código no se lee de la base de datos!! Así que la partida [xxx] y el final [/xxx] son los códigos importantes. Así que si no se pone fin a un BBCode, sólo se contará el texto hasta la etiqueta de apertura.
<br /><br />
Si un usuario edita un mensaje, porque no consiguió puntos anteriores (es decir, antes de instalar el mod o de estar inactivo por un tiempo), el usuario no obtiene puntos por su edición!
<br /><br />
Si un usuario edita un mensaje o tema, donde no consiguió puntos anteriores en la parte de puntos avanzados, este mensaje o tema no se volverá a calcular!'
	),
	array(
		0 => '--',
		1 => 'Configuración de los puntos'
	),
	array(
		0 => 'General',
		1 => 'Aquí puede introducir un nombre diferente para los puntos, activar/desactivar el sistema de puntos y introducir un mensaje de desactivación.
Además se puede activar o desactivar las diferentes partes del sistema de puntos y algunas cosas más, que debe ser auto explicativa.'
	),
	array(
		0 => 'Transferencia a grupo',
		1 => 'Con esta opción en los ajustes principales de los puntos tienes la posibilidad de transferir/quitar una determinada cantidad de puntos a un grupo, o poner a todos el mismo valor. Si usted rellena el asunto y el campo de comentarios, puede enviar un mensaje personal a todos los miembros del grupo. Usted puede utilizar, por supuesto, todos los BBCodes, pero sólo los más comunes en el cuadro.'
	),
	array(
		0 => 'Reiniciar los puntos del usuario',
		1 => 'Con esta opción en los ajustes principales de los puntos, puede restablecer todos los puntos de usuario a cero. ¡Pero cuidado! Esta acción no se puede deshacer!'
	),
	array(
		0 => 'Reiniciar los registros de lso usuarios',
		1 => 'Con esta opción en los ajustes principales puntos, puede restablecer todos los registros de los usuarios. ¡Pero cuidado! Esta acción no se puede deshacer!'
	),
	array(
		0 => '--',
		1 => 'Configuración avanzada de los puntos'
	),
	array(
		0 => 'Adjuntos',
		1 => 'Usted puede dar puntos por temas y mensajes con un archivo adjunto. Los puntos principales se dan de una vez y los puntos adicionales se indican para cada archivo adjunto.
Usted <strong>NO PUEDE</strong> deshabilitar los archivos adjuntos en cada foro base!'
	),
	array(
		0 => 'Encuestas',
		1 => 'Usted puede dar puntos para una encuesta en sí misma (se trata de dar una vez) y puntos para cada opción de la encuesta.
En las encuestas sólo son posibles en nuevos temas, los puntos sólo se dan, si los ajustes de los puntos (> 0) para temas nuevos en la configuración del foro y ha activado el interruptor para temas nuevos!'
	),
	array(
		0 => 'Nuevos temas',
		1 => 'Adicionalmente a los principales puntos por tema de la configuración del foro, se puede dar puntos por cada palabra y/o puntos para cada carácter.
Si se establece en 0 en el foro o se deshabilitan los puntos para temas nuevos, los puntos adicionales no se cuentan!'
	),
	array(
		0 => 'Nuevos mensajes/respuestas',
		1 => 'Adicionalmente a los principales puntos por cada mensaje de la configuración del foro, se puede dar puntos por cada palabra y/o puntos para cada carácter.
Si se establece en 0 en el foro o se deshabilitan los puntos por mensajes nuevos, los puntos adicionales no se cuentan!'
	),
	array(
		0 => 'Puntos por advertencia',
		1 => 'Si se advierte a un usuario, usted tiene la posibilidad de restar puntos de su cuenta. Si el usuario no tiene puntos suficientes, el valor se restará de todos modos. A continuación, tendrá puntos negativos!'
	),
	array(
		0 => 'Puntos por registrarse',
		1 => 'Aquí se puede establecer, la cantidad de puntos que un usuario recibe en su registro en el foro. De esta manera tendrá un capital base. Estos puntos se dan a la vez. Así que no después de activar su cuenta!'
	),
	array(
		0 => 'Entradas por página',
		1 => 'Aquí se puede establecer cuántas entradas de registros y historial de la Lotería se muestran por página. El valor de mínimo es 5.'
	),
	array(
		0 => 'Número de usuarios con más puntos',
		1 => 'Aquí se puede establecer, la cantidad a mostrar de usuarios con más puntos. Verá este número en varios lugares: en el índice, en el banco y en el resumen.
Establece en 0 para desactivar esta función. No serán visibles mas en el índice, y en el banco y en la descripción general, los usuarios verán un mensaje correspondiente.'
	),
	array(
		0 => '--',
		1 => 'Configuración de los puntos para los foros'
	),
	array(
		0 => 'General',
		1 => 'Los puntos para los foros son en su mayoría independientes de la configuración de otros puntos y se contarán adicionalmente. Usted puede establecer los puntos para cada foro. De esta manera usted puede establecer los puntos, que los usuarios recibirán, completamente individual. Va a encontrar estos valores en el Panel de Administración(ACP) - Foros - Administrar Foros - Foro que quieres editar.'
	),
	array(
		0 => 'Los interruptores',
		1 => 'Con los interruptores de los puntos para foros puede activar/desactivar los puntos de foro de nivel global. Si deshabilita los puntos para los temas, los mensajes o editar los puntos no son contados en todos los foros. Además los puntos avanzados no se cuentan, hasta que los active de nuevo.'
	),
	array(
		0 => 'Ajustes globales de los puntos para foros',
		1 => 'Usted puede establecer los puntos de todo el mundo aquí para todos los foros a la vez. Estos ajustes sobrescribirán la configuración previa individual de los puntos! Así que si utiliza esta función, tienes que rehacer todos los foros, donde se definen los diferentes puntos!'
	),
	array(
		0 => 'Nuevo tema',
		1 => 'Aquí se puede establecer, la cantidad de puntos que un usuario recibe un la publicación de un tema nuevo. Se puede configurar de forma global o individual a través del Panel de Administración(ACP) -> Foros.
Si establece 0, también los ajustes avanzados de los puntos (palabras, caracteres) NO cuentan.'
	),
	array(
		0 => 'Nuevo mensaje',
		1 => 'Aquí se puede establecer, la cantidad de puntos que recibe un usuario por la publicación de un mensaje o respuesta nueva. Se puede configurar de forma global o individual a través del Panel de Administración(ACP) -> Foros.
Si establece 0, también los ajustes avanzados de los puntos (palabras, caracteres) NO cuentan.'
	),
	array(
		0 => 'Editar tema/mensaje',
		1 => 'Aquí se puede establecer, si un usuario va a ganar puntos por la edición de un tema o un mensaje.'
	),
array(
		0 => 'Foro capaz de poner costo por los adjuntos',
		1 => 'Aqui se puede establecer si la descarga de adjuntos por los usuarios tendra costes. Los costes se fijaran en Mensajes-adjuntos-manejar extensiones.No se puede si el usuario tendrá los costos por la extensión y si es así, cuánto va a ser!
¡Nota importnte¡ Como sabeis,las imagenes adjuntas se muestran directamente en los mensajes ,por lo que los puntos se restaran directamente. Si un usuario no tiene puntos suficientes ,no podra ver la imagen'
	),
	array(
		0 => '--',
		1 => 'Banco'
	),
	array(
		0 => 'General',
		1 => 'Si se activa el banco, los usuarios verán una ficha adicional en menú principal de Ultimate Points. Además encontrará Información en el perfil y a la vista de un tema, donde administradores/moderadores tienen la posibilidad de modificar los puntos de los usuarios de los puntos y el importe del  banco, si lo permite.'
	),
	array(
		0 => 'Tasa de interés',
		1 => 'Aquí se puede establecer un tipo de interés entre el 0 y el 100 por ciento por cada período de pago. El período de pago se establece como un período "en días". Después de este período los usuarios tendrán su tipo de interés pagado de forma automática. También se puede definir, en qué cantidad de estos pagos se detendrá. Así que tan pronto como un usuario tiene más en su cuenta bancaria, como se ha definido, no recibe ningún pago adicional.'
	),
	array(
		0 => 'Coste de la cuenta',
		1 => 'Aquí se puede establecer el coste por retirar dinero de la cuenta bancaria. Puede establecer cualquier valor entre 0 y 100 por ciento. Además, puede definir un coste fijo por el mantenimiento de la cuenta bancaria en cada período. Éste tendrá el mismo período que la tasa de interés.Por lo tanto los pagos de intereses se efectuarán solamente por el valor establecido.'
	),
	array(
		0 => '--',
		1 => 'Lotería'
	),
	array(
		0 => 'General',
		1 => 'Si se habilita la Lotería, los usuarios tendrán acceso al módulo de la Lotería.
Si deshabilita el módulo, la Lotería aún se ejecuta en segundo plano, pero los usuarios no tienen acceso. La Lotería se ejecutará en el período definido previamente a través de la página de la lotería o de la página del índice.'
	),
	array(
		0 => 'Como funciona la lotería',
		1 => 'Con un cálculo aleatorio, sale un boleto de todos los boletos comprados, es seleccionado como un posible boleto ganador. Después otro cálculo al azar - usando el valor de la oportunidad de ganar - determina si el boleto seleccionado gana realmente o no. Si no gana, el valor va al Bote hasta que salga un boletoganador.'
	),
	array(
		0 => 'Bote',
		1 => 'La Lotería funciona con un sistema de Bote. Así que el valor de todos los boletos comprados al Bote. Además se puede definir un valor inicial para el Bote, que será pagado por estos. Si nadie gana, el Bote se acumula y crecerá cuando se compren mas boletos para el próximo sorteo.'
	),
	array(
		0 => 'Posibilidad de ganar',
		1 => 'Aquí se puede establecer la posibilidad de ganar. Los usuarios no verán este valor. Cuanto mayor sea este valor, mayor es la posibilidad de ganar.
0 significa que nadie va a ganar el Bote, 100 significa que será pagado a uno de los jugadores.'
	),
	array(
		0 => 'Periodo de pago',
		1 => 'Puede establecer el período de pago en horas. Esto tiene un efecto inmediato!
Si se establece el período de pago a 0, el pago se detendrá.
Si los usuarios no pueden comprar boletos el Bote se mantendrá con el el valor actual.
Usted puede utilizar esta característica para obligar a un pago. Tan pronto como se establezca un nuevo valor, el pago comenzará.'
	),
	array(
		0 => 'ID del Remitente',
		1 => 'Aquí se puede establecer el ID del usuario, que enviará un mensaje privado al usuario ganador, avisandole de que ha ganado. Si no desea utilizar un remitente diferente, establezca 0 aquí. Entonces el usuario recibe el mensaje de sí mismo.'
	),
	array(
		0 => '--',
		1 => 'Robo'
	),
	array(
		0 => 'General',
		1 => 'Con el módulo del robo, un usuario puede robar puntos de la cuenta de otros usuarios (del banco no!). Usted puede activar o desactivar el módulo. Si está deshabilitado, los usuarios no verán el módulo.'
	),
	array(
		0 => 'Ajustes de los mensajes personales',
		1 => 'Aquí se puede establecer, si los usuarios son informados sobre el robo. Si el usuario establece en sus ajustes de carácter personal que no quiere recibir mensajes personales de otros usuarios, no se reciben mensajes desde el módulo de robo.
Si el que intenta robar a otro usuario se bloquea el envío de Mensajes Privados, el usuario robado todavía recibe un mensaje. Estos mensajes son pre-definidos y el usuario, que trató de robar, no tiene ninguna influencia en este mensaje..
Cada usuario puede configurar en el módulo interfaz de robo, si a él le gusta ser informado por el modulo robo, si alguien trató de robarle oder si se ha realizado correctamente. Si el envío de la PN se desactiva, el usuario no puede verlo".'
	),
	array(
		0 => 'Posibilidad de éxito del robo',
		1 => 'Aquí se puede establecer el porfentaje de posibilidad de hacer un robo con éxito. Así que usted puede poner cualquier valor entre 0 y 100 por ciento.'
	),
	array(
		0 => 'Penalización por robo fallido',
		1 => 'Aquí se puede establecer la penalización que un usuario tiene que pagar, si falla su robo.
El ladrón tendrá que pagar el porcentage del valor que trató de robar. Lo que un usuario tiene que pagar, si no, se muestra en la página de Robo. Puede establecer cualquier valor entre 0 y 100 por ciento.'
	),
	array(
		0 => 'El valor máximo, que puede ser robado a la vez',
		1 => 'Aquí se puede establecer el porcentaje máximo de los puntos que el usuario robado posee, que puede ser robado a la vez. Este valor se muestra en la página del Robo. Usted puede utilizar cualquier valor entre 0 y 100 por ciento.'
	),
array(
		0 => 'El uso de la pena por robo fallido',
		1 => 'Aquí se puede establecer, lo que le gustaria hacer con la pena de un robo fallido. Puede que quiera directamente la transferencia  del premio mayor de la Lotería o seleccionar al azar, si la pena debe ir a la cuenta de usuario o robado a la del premio mayor de la Lotería.'
	array(

		0 => '--',
		1 => 'Transferir/Donar'
	),
	array(
		0 => 'General',
		1 => 'Si los usuarios tienen el permiso, los usuarios tendrán la posibilidad de transferir los puntos de su cuenta a otra cuenta de los usuarios. Esto se puede hacer desde la página de transferencia, viendo un tema o en el perfil.'
	),
	array(
		0 => 'Mensaje personal con las transferencias',
		1 => 'Usted puede activar o desactivar esta función dentro de la configuración principal de los puntos en la página del Panel de Administración(ACP). Si el usuario tiene bloqueado el envío de Mensajes Privados, no puede añadir un comentario a su transferencia.'
	),
	array(
		0 => 'Registros',
		1 => 'Todo incluido transferencias. todas las informaciones necesarias se muestran en la página de registros. Esta característica puede ser activada/desactivada en la página principal de los ajustes de los puntos. También tiene la posibilidad de restablecer los registros de todos los usuarios. Pero tenga en cuenta, esto no se puede deshacer!'
	),
	array(
		0 => '--',
		1 => 'Permisos'
	),
	array(
		0 => 'Permisos de Administradores',
		1 => 'Usted puede dar al administrador el derecho de administrar Ultimate Points System. Esto se puede hacer en el Panel de Administración(ACP) -> Permisos -> permisos de administrador -> Permisos avanzados.'
	),
	array(
		0 => 'Permisos de Moderadores Globales',
		1 => 'Dentro del Panel de Administración(ACP) -> Permisos -> Moderadores globales usted puede establecer, si tienen permiso para cambiar los puntos y las cuentas bancarias de otros usuarios.'
	),
	array(
		0 => 'Permisos de usuarios y grupos',
		1 => 'Dentro del Panel de Administración(ACP) -> Permisos ->permisos de usuario/grupos, puede poner diferentes  cosas en el sistema de puntos Ultimate. Véase a continuación:
<ul>
	<li>Puede usar los puntos</li> 			
	<li>Puede usar el banco</li>  	 	 	
	<li>Puede usar el módulo de los registros</li> 			
	<li>Puede usar la lotería</li> 			
	<li>Puede usar el robo</li> 			
	<li>Puede usar la transferencia</li>
</ul>'
	),
	array(
		0 => '--',
		1 => 'AddOns y conpatibilidad con otras modificaciones'
	),
	array(
		0 => 'General',
		1 => 'Ultimate Points System es actualmete soportado por otras modificaciones.'
	),
	array(
		0 => 'phpBB Arcade',
		1 => 'El phpBB Arcade de Jeff ( <a href="http://www.jeffrusso.net/">JeffRusso.net</a> ) soporta Ultimate Points System. El Arcade autodetecta, si está instalado el UPS. Puede establecer el conjunto de coste por juegar y un Bote.'
	),
	array(
		0 => 'phpbb Gallery',
		1 => 'La phpBB Gallery de nickvergessen ( <a href="http://www.flying-bits.org/">Flying-bits.org</a> ) soporta Ultimate Points.<br />
Tan pronto como se haya instalado la Galería, podrás ver los campos adicionales en los ajustes principales de los puntos.<br /><br />Usted tendrá que copiar el complemento de conexiones incluida, que se puede encontrar en el paquete de UPS en contrib/addons/Gallery_Integration/root/gallery/includes/hookup_gallery.php a la ubicación correcta en la carpeta de la Galería!<br /><br /><strong>Sugerencia Importante!</strong> Si está utilizando una de las casillas de la imagen (Highslide, Lightbox, Shadowbox.), Los puntos se restan dos veces aquí, debido a un problema técnico. Así que si quieres restar 2 puntos para la visualización de imágenes, usted tiene que introducir 1 punto aquí!<br />Para el cuadro Highslide se puede encontrar una solución para este problema <a href="http://highslide.com/forum/viewtopic.php?p=18498#p18498">aquí</a><br /><br />Además, puede activar/deshabilitar, si el usuario con cero puntos o negativos en la cuenta todavía será capaz de ver las imágenes o no.'
	),
	array(
		0 => 'Medal System MOD',
		1 => 'El Medal System de Gremlinn ( <a href="http://test.dupra.net/">Gremlinn\'s Mod Support Site</a> ) soporta UPS.
Dentro del Panel de Administración(ACP), Mod. Medal, encontrará un campo, donde usted puede establecer, la cantidad de puntos que un usuario recibe adicionalmente por recinbir una medalla.'
	),
	array(
		0 => 'Sudoku',
		1 => 'El Sudoku MOD de el_teniente ( <a href="http://vfalcone.ru/">vfalcone.ru</a> ) soporta UPS.
A pesar de que todavía tienen un sistema interno de puntos, puede establecer los puntos, que los usuarios recibirán en el sistema de recompensas.'
	),
	array(
		0 => 'F1 Webtipp',
		1 => 'El Fórmula 1 Webtipp de Dr.Death ( <a href="http://www.lpi-clan.de/">LPI-Clan</a> ) soporta UPS.
Puede configurar los puntos que los usuarios recibirán por sus pronosticos.'
	),
	array(
		0 => 'DM Video',
		1 => 'El DM Video MOD de femu ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ) soporta UPS.
Puede establecer allí, cuántos puntos recibiran los usuarios , cuando añaden un video. Esta cantidad se restará, por supuesto, cuando el usuario elimina un vídeo.'
	),
	array(
		0 => 'UPS Easy Shop',
		1 => 'El UPS Easy Shop ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ) funciona con Ultimate Points. El usuario puede comprtar objetos y listarlos en su perfil y en la lista de temas.<br />
Hay puglins para que los usuarios puedan comprar colores o mensajes.'
	),
	array(
		0 => 'User Blog Mod',
		1 => 'El User Blog Mod de EXreaction ( <a href="http://www.lithiumstudios.org/">Lithiumstudios.org</a> ) soporta UPS.

Para utilizar el Mod blog del usuario con UPS, tiene que instalen el plugin, que se puede encontrar en la carpeta contrib/addons/User_Blog_Mod_Plugin.
Después de la instalación encontrará ajustes adicionales en la configuración del blog Mod.'
	),
	array(
		0 => 'Board3 Portal',
		1 => 'Usted encontrará en la carpeta contrib/AddOns/Board3_Portal_AddOns un addon, que mostrará la lotería en la página Portal Board3 ( <a href="http://www.board3.de/">Board3 Portal</a> ).'
	),
	array(
		0 => 'DM Easy Download System',
		1 => 'El DM EDS from femu ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ) soporta Ultimate Points.<br />El DM EDS simple sistema de descargas, donde se pueden establecer los costes para cada descarga por separado. Así que si los usuarios si no tienen suficientes puntos, no puede descargar los archivos.'
	),
	array(
		0 => 'DM Quotes Collection',
		1 => 'DM Quotes Collection es una simple herramienta de femu ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ), en donde usted tiene la posibilidad de empezar a crear una colección de citas, que se mostrarán en un orden aleatorio en el índice. Todas las citas se gestionan a través del Panel de Administración(ACP). Tan pronto como se aprueba una cita, el usuario recibirá los puntos, que se establecieron en el Panel de Administración(ACP).'
	),
	array(
		0 => 'Knuffel (Dice role game)',
		1 => 'Ultimate Points soporta Knuffel (un clon del juego Kniffel) de Wuerzi ( <a href="http://www.spieleresidenz.de/">Spieleresidenz</a> ). Dentro de este juego usted necesita diferentes conjuntos de datos de dados para obtener el máximo de puntos. En el Panel de Administración(ACP) se pueden establecer el coste por jugar y también establecer un Bote.'
	),
	array(
		0 => 'Invite A Friend',
		1 => 'El Invite A Friend Mod de Bycoja ( <a href="http://bycoja.by.funpic.de/">Bycoja\'s Bugs</a> ) en la version 0.5.3 soporta Ultimate Points.
. Invite A Friend es una adición para phpBB3, con lo que habilitas a los usuarios para invitar a nuevos amigos.'
	),
	array(
		0 => 'Hangman',
		1 => 'El juego Hangman ( <a href="http://area53.die-muellers.org/">Our Mod Support Site</a> ) soporta Ultimate Points. Con este juego los usuarios podrán ganar puntos mediante la publicación de Hangmans o adivinar las palabras.'
	),	
	array(
		0 => 'DM Music Charts',
		1 => ' DM Music Charts from femu ( <a href="http://area53.die-muellers.org/">femu\'s Mod Support Site</a> ) soporte ultimate poinst. Con este mod puede ofrecer a sus usuarios una mesa de cartas, que es llenado por los usuarios. Así que todos los usuarios registrados pueden agregar nuevas canciones.'
	),
	array(
		0 => 'phpBB Football',
		1 => 'phpBB Football( <a href="http://football.bplaced.net//">football\'s Support aund Demo Site</a> ) soporta ultimate points. Con esta modificación se pueden gestionar diferentes ligas y ofrecer a sus usuarios una página de apuestas.'
	),
);

?>