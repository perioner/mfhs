<?php
	// ======================================== \
	// Package: Mihalism Multi Forum Host 
	// Version: 3.0.0
	// Copyright (c) 2007, 2008 Mihalism, Inc.
	// License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GNU Public License
	// ======================================== /
	
	if (is_file("{$mfh_root_path}source/includes/data.php") == false || preg_match("/([a-zA-Z0-9])/", str_replace("/", "\/", $mfh_root_path)) || $mfh_root_path == NULL) {
		exit("Stop trying to hack my site {$_SERVER['REMOTE_ADDR']}");
	}

	require_once "{$mfh_root_path}source/includes/data.php";

	if ($mfhclass->funcs->forum_exists($mfhclass->input->get_vars['access_name']) == false) {
		header("Location: ../../index.php");
		exit;
	} else {
		$mfhclass->info->forum_info = $mfhclass->db->fetch_array($mfhclass->db->query("SELECT * FROM `mfh_hosted_forums` WHERE `access_name` = '{$mfhclass->input->get_vars['access_name']}';"));

		if ($mfhclass->funcs->is_null($mfhclass->input->get_vars['r']) == false && $mfhclass->input->get_vars['r'] == $mfhclass->info->forum_info['total_hits']) {
			$mfhclass->db->query("UPDATE `mfh_hosted_forums` SET `total_hits` = `total_hits` + 1 WHERE `access_name` = '{$mfhclass->info->forum_info['access_name']}';");
		}
		
		$mfhclass->info->forum_info['database'] = $mfhclass->db->fetch_array($mfhclass->db->query("SELECT * FROM `mfh_forum_databases` WHERE `database_id` = '{$mfhclass->info->forum_info['database_id']}';"));
		unset($mfhclass->info->forum_info['database_id']);

		// phpBB 3.0.x auto-generated configuration file
		// Do not change anything in this file!
		$dbms = "mysql";
		$dbhost = "{$mfhclass->info->forum_info['database']['sql_host']}";
		$dbport = "";
		$dbname = "{$mfhclass->info->forum_info['database']['sql_database']}";
		$dbuser = "{$mfhclass->info->forum_info['database']['sql_username']}";
		$dbpasswd = "{$mfhclass->info->forum_info['database']['sql_password']}";

		$table_prefix = "{$mfhclass->info->forum_info['access_name']}_";
		$acm_type = "file";
		$load_extensions = "";

		@define("PHPBB_INSTALLED", true);
		// @define("DEBUG", true);
		// @define("DEBUG_EXTRA", true);
	}

?>
