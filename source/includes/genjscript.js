// ======================================== \
// Package: Mihalism Multi Forum Host 
// Version: 3.0.0
// Copyright (c) 2007, 2008 Mihalism, Inc.
// License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GNU Public License
// ======================================== /

var page_url         = location.href;
var google_account   = "UA-1125794-7"; // <- Google Analytics tracker ID
var preloaded_images = new Array();
var preloaded_count  = 0;

preload_image("css/images/progress_bar.gif", 280, 16);

_uacct = google_account; 
urchinTracker()

function fetchElementById(id) 
{ 
	if (document.getElementById) {
		var return_var = document.getElementById(id); 
	} else if (document.all) {
		var return_var = document.all[id]; 
	} else if (document.layers) { 
		var return_var = document.layers[id]; 
	} else {
		alert("Failed to fetch element ID '" + id + "'");
	}
	return return_var; 
}

function preload_image(url, width, height)
{
	if (document.images) {
		preloaded_count++;
		preloaded_images[preloaded_count] = new Image(width, height); 
		preloaded_images[preloaded_count].src = url;
	}
	return true;
}

function toggle(id) {
	var block_id = fetchElementById(id);

	if (block_id.style == false) {
		block_id.setAttribute("style", "");
	}
		
	block_id.style.display = ((block_id.style.display == "none") ? "block" : "none");

	return true;
}

function highlight(field) 
{
       	field.focus();
       	field.select();
	return true;
}
