<?php
	// ======================================== \
	// Package: Mihalism Multi Forum Host
	// Version: 3.0.0
	// Copyright (c) 2007, 2008 Mihalism, Inc.
	// License: http://www.gnu.org/licenses/gpl.txt GNU Public License
	// ======================================== /
	
	/*
		Initialization file for Mihalism Multi Forum Host
		
		MFHCLASS v2.0.1
		
		Developed by: Michael, Craig, Ahmed, and Eugene.
	*/
	clearstatcache();

	$mfhclass = new stdClass; // <-- Just make it look like a class

	ini_set("display_errors"  , 0);
	ini_set("log_errors"      , 0);
	ini_set("register_globals", 0);
	ini_set("memory_limit"    , "128M");
	ini_set("post_max_size"   , "128M");
	
	if (__FILE__ == NULL) {
		exit("<b>Error</b>: <br /><br /> __FILE__ is NULL <br /><br /> No se puede lanzar la instalacion o la Aplicacion");
	}
	
	$mfhclass->info->root_path = (dirname(preg_replace("#/source/includes/#i", "/", str_replace("\\", "/",  __FILE__)))."/"); //<-- YAH! Now works on Windows
	
	require_once "{$mfhclass->info->root_path}source/includes/config.php";
	require_once "{$mfhclass->info->root_path}source/includes/functions.php";
	require_once "{$mfhclass->info->root_path}source/includes/database.php";
	require_once "{$mfhclass->info->root_path}source/includes/template.php";

	$mfhclass->db    = new mfhclass_mysql_driver();
	$mfhclass->templ = new mfhclass_template_engine();
	$mfhclass->funcs = new mfhclass_core_functions();

	$mfhclass->input->get_vars    = $mfhclass->funcs->clean_array($_GET);    // <-- MySQL Safe _GET Variable
	$mfhclass->input->file_vars   = $mfhclass->funcs->clean_array($_FILES);  // <-- MySQL Safe _FILES Variable
	$mfhclass->input->post_vars   = $mfhclass->funcs->clean_array($_POST);   // <-- MySQL Safe _POST Variable
	$mfhclass->input->server_vars = $mfhclass->funcs->clean_array($_SERVER); // <-- MySQL Safe _SERVER Variable
	$mfhclass->input->cookie_vars = $mfhclass->funcs->clean_array($_COOKIE); // <-- MySQL Safe _COOKIE Variable

	$mfhclass->info->phpbb_version = "3.0.1"; // <-- DO NOT CHANGE !
	$mfhclass->info->base_url      = $mfhclass->funcs->fetch_url(false, false, false);
	$mfhclass->info->page_url      = $mfhclass->funcs->fetch_url(true, false, true);
	$mfhclass->info->script_path   = ((dirname($mfhclass->input->server_vars['php_self']) != "/") ? (dirname($mfhclass->input->server_vars['php_self'])."/") : dirname($mfhclass->input->server_vars['php_self']));
	$mfhclass->info->current_page  = round(($mfhclass->funcs->is_null($mfhclass->input->get_vars['page']) == false && $mfhclass->input->get_vars['page'] >= 1) ? $mfhclass->input->get_vars['page'] : 1);

	if (version_compare(phpversion(), "5.0.0", ">=") == false) { 
		$mfhclass->templ->fatal_error("Tu Version de PHP no es Compatible con MHFS{$mfhclass->info->version}");
	} elseif (extension_loaded("mysql") == false) {
		$mfhclass->templ->fatal_error("Lo siento una extension no se pudo cargar {$mfhclass->info->version} o mysql extension no esta funcionando .");
	}
	
	if ($mfhclass->info->site_installed == false) {
		if (preg_match("/install/", basename($mfhclass->input->server_vars['php_self'])) == false) {
			$mfhclass->templ->page_title = "Installation Required";
			$mfhclass->templ->error("This website has yet to be installed. Please click <a href=\"install.php\">here</a> to continue to installation.", true);
		}
	} else {
		$do_not_null = array("do_not_null", "mfhclass", "mfh_root_path", "phpEx", "phpbb_root_path", "HTTP_SERVER_VARS", "GLOBALS", "_GET", "_POST", "_COOKIE", "_REQUEST", "_SERVER", "_SESSION", "_ENV", "_FILES");
		foreach ($GLOBALS as $variable_name => $variable_value) {
			if (in_array($variable_name, $do_not_null) == false) {
				$$variable_name = NULL;
			}
		}

		$sql = $mfhclass->db->query("SELECT * FROM `mfh_site_settings`;");
		while ($row = $mfhclass->db->fetch_array($sql)) {
			$mfhclass->info->config[$row['config_key']] = $row['config_value'];
		}
	}

?>
