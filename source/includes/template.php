<?php
	// ======================================== \
	// Package: Mihalism Multi Forum Host
	// Version: 3.0.0
	// Copyright (c) 2007, 2008 Mihalism, Inc.
	// License: http://www.gnu.org/licenses/gpl.txt GNU Public License
	// ======================================== /
	
	/* Template Parser: v1.0.2 */
	
	class mfhclass_template_engine
	{
		function mfhclass_template_engine()
		{
			$this->templ_vars = array(); 
			$this->templ_globals = array();
			$this->templ_html = array();
			return;
		}
	
		function output($filename = NULL, $template = NULL)
		{
			global $mfhclass;
			$template_html  = $this->page_header();
			$template_html .= (($mfhclass->funcs->is_null($this->html) == false) ? $this->html : $this->parse_template($filename, $template));
			$template_html .= $this->page_footer();
			$template_html .= base64_decode("PCEtLSBQb3dlcmVkIGJ5IE1paGFsaXNtIE11bHRpIEhvc3QgLSBDb3B5cmlnaHQgKGMpIDIwMDcsIDIwMDggTWloYWxpc20sIEluYy4gKHd3dy5taWhhbGlzbS5jb20pIC0tPg==");
			$mfhclass->db->close();
			exit($template_html);
			return;
		}
		
		function parse_template($filename, $template = NULL) {
			global $mfhclass;
			
			if (is_file("{$mfhclass->info->root_path}source/public_html/{$filename}.tpl") == false) {
				exit("<b>Fatal Error</b>: The template '{$filename}.tpl' does not exist.");
			} else {
				$html2parse = implode("", file("{$mfhclass->info->root_path}source/public_html/{$filename}.tpl"));
				
				$html2parse = preg_replace("#<!-- (BEGIN|END): (.*) -->#", NULL, $html2parse);
				$html2parse = preg_replace("#<\\$(.*?)\\$>#Us", NULL, $html2parse);
				$html2parse = preg_replace(array('#<([\?%])=?.*?\1>#s', '#<script\s+language\s*=\s*(["\']?)php\1\s*>.*?</script\s*>#s', '#<\?php(?:\r\n?|[ \n\t]).*?\?>#s'), NULL, $html2parse);
			
				if (strstr($html2parse, "<template id=") == true) {
					$html2parse = trim(preg_replace("#</template>(.*)<template#Us", "</template>\n<template", $html2parse));
				
					/* Parse <template> tag into the templ_html array */
					preg_match_all("#<template id=\"(.*)\">#", $html2parse, $template_matches);
					foreach ($template_matches['1'] as $template_id) {
						$html2parse = preg_replace("#<template id=\"{$template_id}\">(.*)</template>#Us", "<"."?php\n\tob_start();\n?>$1<"."?php\n\t\$this->templ_html['{$filename}.{$template_id}'] = ob_get_clean();\n?".">", $html2parse);
					}
					$template_preg = preg_match("#<\?php(?:\r\n?|[ \n\t])(.*?)\?>#Us", $html2parse, $preg_matches);
					eval($preg_matches['1']);
					
					if (array_key_exists("{$filename}.{$template}", $this->templ_html) == false) {
						exit("<b>Fatal Error</b>: Template ID '{$template}' does not exist.");
					} else {
						$html2parse = $this->templ_html["{$filename}.{$template}"];
					}
				}
				
				if (empty($this->templ_vars) == false) {
					for ($i = 0; $i < count($this->templ_vars); $i++) {
						foreach ($this->templ_vars[$i] as $variable => $replacement) {
							if (preg_match("#<\# {$variable} \#>#", $html2parse) == true) {
								$html2parse = str_replace("<# {$variable} #>", $replacement, $html2parse);
								unset($this->templ_vars[$i][$variable]);
							}
						}
					}
				}
				
				if (strstr($html2parse, "<foreach=") == true) {
					$parse_html2php = true;
					$html2parse = preg_replace("#<foraech=\"(.*)\">#", '<?php foreach ($1) { ?>', $html2parse);
					$html2parse = preg_replace("#</endforeach>#", '<?php } ?>', $html2parse);
				}
				
				if (strstr($html2parse, "<if=") ==  true) {
					$parse_html2php = true;
					$html2parse = preg_replace("#<if=\"(.*)\">#", '<?php if ($1) { ?>', $html2parse);
					$html2parse = preg_replace("#<elseif=\"(.*)\">#", '<?php } elseif ($1) { ?>', $html2parse);
					$html2parse = preg_replace("#<else>#", '<?php } else { ?>', $html2parse);
					$html2parse = preg_replace("#</endif>#", '<?php } ?>', $html2parse);
				}
				
				if (strstr($html2parse, "<php>") == true) {
					$parse_html2php = true;
					$html2parse = preg_replace("#<php>#", '<?php', $html2parse);
					$html2parse = preg_replace("#</php>#", '?>', $html2parse);
				}	
				
				if (strstr($html2parse, "<while id=") == true) {
					preg_match("#<while id\=\"(.*)\">(.*)</endwhile>#Us", $html2parse, $whileloop_matches);
					if ($this->templ_globals['get_whileloop'] == false) {
						$html2parse = preg_replace("#<while id\=\"{$whileloop_matches['1']}\">(.*)</endwhile>#Us", $this->templ_globals[$whileloop_matches['1']], $html2parse);
					} else {
						$html2parse = $whileloop_matches['2'];
					}
				}	
				
				if ($parse_html2php == true) {
					ob_start();
					eval("?".">".$html2parse);
					$html2parse = ob_get_clean();
				}	
				
				// I like WWE Monday Night Raw, do you ?
				
				return $html2parse;
			}
		}

		function page_header()
		{
			global $mfhclass;
			if ($mfhclass->funcs->is_null($this->page_header) == true) {
				$this->templ_vars[] = array(
					"PAGE_TITLE"       => (($mfhclass->funcs->is_null($this->page_title) == false) ? $this->page_title : "Welcome to {$mfhclass->info->config['site_name']} (Free phpBB Forum Hosting)"),
					"SHORT_PAGE_TITLE" => (($mfhclass->funcs->is_null($this->page_title) == false) ? $this->page_title : $mfhclass->info->config['site_name']),
					"SITE_NAME"        => $mfhclass->info->config['site_name'],
					"EMAIL_OUT"        => $mfhclass->info->config['email_out'],
				);
				return $this->parse_template("page_header");
			} else {
				return $this->page_header;
			}
		}

		function page_footer()
		{
			global $mfhclass;
			if ($mfhclass->funcs->is_null($this->page_footer) == true) {
				return $this->parse_template("page_footer");
			} else {
				return $this->page_footer;
			}
		}

		function error($error, $output_html = true)
		{
			$template_html = "<div class=\"errorbox\"><h3>Warning</h3><p>{$error}</p></div>";
			if ($output_html == true) {
				$this->html = $template_html;
				$this->output();
				return;
			} else {
				return $template_html;
			}
		}

		function success($message, $output_html = false)
		{
			$template_html = "<div class=\"successbox\"><h3>Information</h3><p>{$message}</p></div>";
			if ($output_html == true) {
				$this->html = $template_html;
				$this->output();
				return;
			} else {
				return $template_html;
			}
		}

		function pagelinks($base_url, $total_results)
		{
			global $mfhclass;
			$total_pages  = ceil($total_results / $mfhclass->info->config['max_results']); 
			$base_url     = ($base_url.((preg_match("/\?/", $base_url) == true) ? "&amp;" : "?"));
			$current_page = (($mfhclass->info->current_page > $total_pages) ? $total_pages : $mfhclass->info->current_page); 
			if ($total_pages < 2) {
				$template_html = "Viewing Only Page";
			} else {
				$template_html .= (($current_page > 1) ? "<a href=\"{$base_url}page=".($mfhclass->info->current_page - 1)."\">&laquo; Previous</a>" : NULL);
				for ($i = 0; $i < $total_pages; $i++) {
					$this_page = ($i + 1);
					if ($this_page == $current_page) {
						$template_html .= "<strong>{$this_page}</strong>";
					} else {
						if ($this_page < ($current_page - 5)) {
							continue;
						}
						if ($this_page > ($current_page + 5)) {
							break;
						}
						$template_html .= ("<a href=\"{$base_url}page={$this_page}\">".$mfhclass->funcs->format_number($this_page)."</a>");
					}
				}
				$template_html .= (($current_page < $total_pages) ? ("<a href=\"{$base_url}page=".($mfhclass->info->current_page + 1)."\">Next &raquo;</a>") : NULL);
				$template_html  = "Page {$current_page} of {$total_pages} &bull; {$template_html}";
			}
			return "<div class=\"pagination\">Pages: {$template_html}</div>";
		}
		
		function fatal_error($error) {
			exit("\t\t\t<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">
			<html>
				<head>
					<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
					<title>MySQL Error (Powered by Mihalism Multi Forum Host)</title>
					<style type=\"text/css\">
					    	* { font-size: 100%; margin: 0; padding: 0; }
						body { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 75%; margin: 10px; background: #FFFFFF; color: #000000; }
						a:link, a:visited { text-decoration: none; color: #005fa9; background-color: transparent; }
						a:active, a:hover { text-decoration: underline; }						
						textarea { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; border: 1px dashed #000000; background: #FFFFFF; padding: 5px; background: #f4f4f4; }
					</style>
				</head>
				<body>
					<p><b>Fatal Error</b>
					<br /><br />
					{$error}
					<br /><br />
					Application Exited
				</body>
			</html>");
			return;
		}	
	}

?>
