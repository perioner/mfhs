<?php
	// ======================================== \
	// Package: Mihalism Multi Forum Host
	// Version: 3.0.0
	// Copyright (c) 2007, 2008 Mihalism, Inc.
	// License: http://www.gnu.org/licenses/gpl.txt GNU Public License
	// ======================================== /

	class mfhclass_mysql_driver
	{
		function connect($host = "localhost", $username, $password, $database, $new_link = NULL)
		{
			global $mfhclass;
			$connection_id = mysql_connect($host, $username, $password, true);
			
			if (is_resource($connection_id) == false) {
				$this->error();
			} else {
				if (mysql_select_db($database, $connection_id) == false) {
					$this->error();
				} else {
					if (is_resource($this->root_connection) == false) {
						$this->root_connection = $connection_id;
					} else {
						if ($mfhclass->funcs->is_null($new_link) == false) {
							if (is_array($this->alt_connections) == true) {
								$this->alt_connections = array();
							}
					
							$this->alt_connections[$new_link] = $connection_id;
						}
					}
				}
			}
			
			return $connection_id;
		}

		function close()
		{
			if (is_resource($this->root_connection) == true) {
				mysql_close($this->root_connection);
			}
			
			if (is_array($this->alt_connections) == true) {
				foreach ($this->alt_connections as $id => $connection) {
					mysql_close($this->alt_connections[$id]);
				}
			}
		}
			
		function set_database_connection($database_id = 1) {
			global $mfhclass;
			if ($database_id != 1 && $mfhclass->funcs->is_null($database_id) == false) {
				$database_info = $this->fetch_array($this->query("SELECT * FROM `mfh_forum_databases` WHERE `database_id` = '{$database_id}';"));
				$this->connect($database_info['sql_host'], $database_info['sql_username'], $database_info['sql_password'], $database_info['sql_database'], $database_info['database_id']);
			}
		}

		function query($query, $connection_id = NULL)
		{
			global $mfhclass;
			if (is_resource($this->root_connection) == false) {
				$this->connect($mfhclass->info->config['sql_host'], $mfhclass->info->config['sql_username'], $mfhclass->info->config['sql_password'], $mfhclass->info->config['sql_database']);
			}
			
			$query = str_replace("<# QUERY_LIMIT #>", ((($mfhclass->info->current_page * $mfhclass->info->config['max_results']) - $mfhclass->info->config['max_results']).", {$mfhclass->info->config['max_results']}"), $query);
			if ($mfhclass->info->config['sql_tbl_prefix'] != "mmh_" && $mfhclass->funcs->is_null($mfhclass->info->config['sql_tbl_prefix']) == false){
				$query = preg_replace("/mmh_(\S+?)([\s\.,]|$)/", ($mfhclass->info->config['sql_tbl_prefix']."\\1\\2"), $query);
			}
			
			if ($mfhclass->funcs->is_null($connection_id) == false && $connection_id != 1) {
				$this->set_database_connection($connection_id);
				if (is_resource($this->alt_connections[$connection_id]) == false) {
					$this->error($query, "Unknown alternate connection id: {$connection_id}");
				}
			}
			
			$this->query_result = mysql_query($query, (($mfhclass->funcs->is_null($connection_id) == false && $connection_id != 1) ? $this->alt_connections[$connection_id] : $this->root_connection));

			if ($this->query_result == false) {
				$this->error($query);
			} else {
				return $this->query_result;
			}
		}

		function total_rows($query_id)
		{
			return mysql_num_rows($query_id);
		}

		function fetch_array($query_id, $result_type = MYSQL_ASSOC)
		{
			return mysql_fetch_array($query_id, $result_type);
		}

		function error_number()
		{
			global $mfhclass;
			return (($mfhclass->funcs->is_null(mysql_error()) == false) ? mysql_errno() : "Unknown Error Number");
		}

		function error($query = "No Query Executed", $custom_error = NULL)
		{
			global $mfhclass;
			$error_message = (($mfhclass->funcs->is_null($custom_error) == false) ? $custom_error : mysql_error());
			$error_html = "\t\t\t<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">
			<html>
				<head>
					<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
					<title>MySQL Error (Powered by Mihalism Multi Forum Host)</title>
					<style type=\"text/css\">
					    	* { font-size: 100%; margin: 0; padding: 0; }
						body { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 75%; margin: 10px; background: #FFFFFF; color: #000000; }
						a:link, a:visited { text-decoration: none; color: #005fa9; background-color: transparent; }
						a:active, a:hover { text-decoration: underline; }						
						textarea { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; border: 1px dashed #000000; background: #FFFFFF; padding: 5px; background: #f4f4f4; }
					</style>
				</head>
				<body>
					<p>
						<b>MySQL Driver Error</b>
						<br /><br />
						A MySQL error has occurred. 
						Please copy the output shown below and email it immediately to <a href=\"mailto:{$mfhclass->input->server_vars['server_admin']}\">{$mfhclass->input->server_vars['server_admin']}</a>.
						<br /><br />
						<textarea readonly=\"readonly\" rows=\"15\" cols=\"40\" style=\"width:500px;\">Time Encountered: ".date("F j, Y, g:i:s a")."\nIP Address: {$mfhclass->input->server_vars['remote_addr']}\rError: {$error_message}\nError Number: ".$this->error_number()."\nQuery Executed: {$query}</textarea>
					</p>		
				</body>
			</html>";
			exit($error_html);
			return;
		}
	}

?>
