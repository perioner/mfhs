<!-- BEGIN: INSTALLER INTRO PAGE -->
<template id="installer_intro_page">

<h1>Bienvenido a la Instalacion de MHFS (Multi Forum Hosting Script)</h1><br />
<p>
	Bienvenido a la Instalacion de MHFS (Multi Forum Hosting Script) <i>Zero-Proyect.net.ms</i>, 
	La idea original del proyecto fue de Mishalim MultiForum Host<i>Mihalism, Inc.</i> 
	Para dar la oportunidad de que todos puedan tener su propio hosting de Foros .
	El Script es Open Source es Decir <b>Gratuito</b>, y es de Facil Uso a continuacion le diremos
	lo que trae y necesita
	<br /><br />
	<p>El Original Mishalim Multi Forum Host Trae la version de <b>phpBB : 3.0.1 </b>.</p>
	<p>Nuestro Script Actualizado funciona ya con la version de <b>phpBB : <# PACKAGED_PHPBB_VERSION #> </b></p>
	* Cada Dia mejoramos y intentamos poder mejorar dia a dia con ustedes 
	<br /><br />
	Los Requesitos para hacer Funcionar Multi Forum Host Script Son los Siguientes:
	<br /><br />
	&nbsp;&nbsp;1. <a href="http://httpd.apache.org/" target="_blank">Servidor Web Apache</a><br />
	&nbsp;&nbsp;&nbsp;2. <a href="http://www.mysql.com/" target="_blank">Servidor de MySQL</a><br />
	&nbsp;&nbsp;&nbsp;&nbsp;3. <a href="http://www.php.net/" target="_blan">PHP: Hypertext Preprocessor y con la Version (5.0.0) [Recomendado] </a><br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. <a href="http://httpd.apache.org/docs/2.0/mod/mod_rewrite.html" target="_blank">Apache mod_rewrite Module</a></li>
	<br /><br />
	<b>Peligro:</b> Si ya tenia una version de MHFS (Multi Forum Hosting Script) , esta instalacion lo borrara 
	<br /><br />
	Presione en Continuar la Instalacion y Configure su Foro.
	<br /><br />
	<fieldset class="submit-buttons">
		<a href="install.php?act=install" class="button1">Continuar la Instalacion</a>
		<a href="" class="button1">Foros de Soporte</a>
		<a href="changelog.html" class="button1">Changelog</a>
	</fieldset>
</p>

</template>
<!-- END: INSTALLER INTRO PAGE -->
<!-- BEGIN: INSTALL FORM PAGE -->
<template id="install_form_page">

<form method="post" action="install.php?act=install-d">
	<h1>Formulario de Instalacion</h1><br />
	<p>
		Para continuar con la Instalacion debera rellenar por Completo este Formulario con los datos de su Hosting .
		<fieldset>
			<legend>Conexión a la Base de Datos</legend>
			<dl>
				<dt><label for="sql_host">Servidor MYSQL (HostName):</label><br /><span class="explain">Su Servidor de mysql en algunos host es "s1.tuhost.com" y en otros "localhost".</span></dt>
				<dd><input id="sql_host" type="text" size="25" name="sql_host" value="editame" /></dd>
			</dl>
			<dl>
				<dt><label for="sql_database">Base de Datos (Nombre):</label><br /><span class="explain">Para continuar la Instalacion debera crear una base de datos con cualquier nombre y editar el campo , gracias a esta base de datos el script creara los foros . </span></dt>
				<dd><input id="sql_database" type="text" size="25" name="sql_database" value="crea la tuya" /></dd>
			</dl>
			<dl>
				<dt><label for="sql_username">Usuario Mysql:</label></dt>
				<dd><input id="sql_username" type="text" size="25" name="sql_username" value="root" /></dd>
			</dl>
			<dl>
				<dt><label for="sql_password">Contraseña Mysql:</label><br /><span class="explain">Si no tienes password dejalo en blanco.</span></dt>
				<dd><input id="sql_password" type="password" size="25" name="sql_password" value="" /></dd>
			</dl>
		</fieldset>
		<fieldset>
			<legend>Configuracion Panel de Control</legend>
			<dl>
				<dt><label for="username">Usuario Administador:</label><br /><span class="explain"></span></dt>
				<dd><input id="username" type="text" size="25" maxlength="30" name="username" value="Admin" /></dd>
			</dl>
			<dl>
				<dt><label for="password">Constraseña:</label><br /><span class="explain">.</span></dt>
				<dd><input id="password" type="password" size="25" maxlength="30" name="password" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="password-c">Contraseña (Repetir):</label></dt>
				<dd><input id="password-c" type="password" size="25" maxlength="30" name="password-c" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="email_address">E-mail:</label><br /><span class="explain"></span></dt>
				<dd><input id="email_address" type="text" size="25" name="email_address" value="<# SERVER_ADMIN #>" /></dd>
			</dl>
		</fieldset>
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" onclick="this.className = 'button1 disabled';" value="Continue la Instalacion" />
		</fieldset>
	</p>
</form>

</template>
<!-- END: INSTALL FORM PAGE -->
