<!-- // -- |ACPT-BE| -- //  -->
<template id="admin_login_page">

<form method="post" action="admin.php?act=login-f">
	<fieldset>
		<legend>Entrada del Administrador</legend>
		<dl>
			<dt><label for="username">Usuario:</label></dt>
			<dd><input id="username" type="text" size="25" maxlength="30" name="username" value="" /></dd>
		</dl>
		<dl>
			<dt><label for="password">Contrase�a:</label></dt>
			<dd><input id="password" type="password" size="25" maxlength="30" name="password" value="" /></dd>
		</dl>
	</fieldset>
	<fieldset class="submit-buttons">
		<input class="button1" type="submit" value="Log In" />
	</fieldset>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="admin_index_page">

<h1>Admin CP Index</h1><br />
<p>
	Bienvenido al Panel de control de administraci�n de anfitri�n del Foro Mihalism Multi. No se cuenta con mucho disponible en el momento
esto fue escrito, pero eso es porque esta es una de las primeras versiones de anfitri�n del Foro Mihalism Multi. Usted puede esperar mucho
m�s en el futuro pero por ahora hay algunas herramientas b�sicas para ayudarle a manejar sus foros alojados.
<# PAGINATION_LINKS #>
	<table cellspacing="1">
		<thead>
			<tr>
				<th>Nombre de acceso</th>
				<th>Total Clicks</th>
				<th>Fecha de Creacion</th>
				<th>Total Miembros</th>
				<th>Dias sin ningun post</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<while id="forum_listing_whileloop">
				<tr class="<# TRCLASS #>">
					<td><a target="_blank" href="<# BASE_URL #>forums/<# ACCESS_NAME #>/"><# ACCESS_NAME #></a></td>
					<td><# TOTAL_HITS #></td>
					<td><# DATE_CREATED #></td>
					<td><# TOTAL_MEMBERS #></td>
					<td><# DAYS_WITHOUT_POST #> Day(s)</td>	
					<td><a href="admin.php?act=forum_settings&access_name=<# ACCESS_NAME #>">Edit Settings</a> | <a href="admin.php?act=remove_forum&access_name=<# ACCESS_NAME #>">Delete</a></td>
				</tr>
			</endwhile>
		</tbody>
	</table>
</p>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="database_manager_index_page">

<h1>Servidores de Base de Datos</h1> <br />
<p>
El anfitri�n Mihalism Multi Foro gestor de bases de ofrecerle la posibilidad de ampliar el almacenamiento
capacidades de su foro de alojamiento, ya que permite la oportunidad de a�adir m�ltiples
bases de datos para almacenar los datos en el foro.
	<table cellspacing="1">
		<thead>
			<tr>
				<th>#</th>
				<th>Servidor MySql</th>
				<th>Base de Datos Nombre</th>
				<th>MySQL Usuario</th>
				<th>MySQL Contrase�a</th>
				<th>Nuevas Altas</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<while id="database_manager_listing_whileloop">
				<tr class="<# TRCLASS #>">
					<td><# DATABASE_ID #></td>
					<td><# DATABASE_HOST #></td>
					<td><# DATABASE_NAME #></td>
					<td><# DATABASE_USERNAME #></td>
					<td><# DATABASE_PASSWORD #></td>
					<td><# ALLOW_SIGNUPS #></td>	
					<td><a href="admin.php?act=database-e&db_id=<# DATABASE_ID #>">Edit Settings</a> | <a href="admin.php?act=database-r&db_id=<# DATABASE_ID #>">Remove</a></td>
				</tr>
			</endwhile>
		</tbody>
	</table>
	<fieldset class="quick" style="float: left;">
		<a class="button2" href="admin.php?act=database-n">A�adir Base de Datos</a>
	</fieldset>
</p>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="delete_database_page">

<fieldset>
	<legend>Borrar Base de Datos</legend>
	<form action="admin.php?act=database-r-d" method="post">
		<h1>Confirmar Borrado</h1><br />
		<p>
			Esta seguro de borrar la Base de Datos? 
			<br /><br />
			Si le da a si no habra marcha atras.
			<br /><br />
			<strong>Nota</strong>: Eliminaci�n de una base de datos s�lo se eliminar� de la lista de bases de datos disponibles,
No elimine la propia base de datos o cualquiera de los contenidos dentro de la base de datos real.
		</p>
		<input type="hidden" name="db_id" value="<# DATABASE_ID #>">
		<div style="text-align: center;">
			<input type="submit" value="Yes" class="button2" />&nbsp; 
			<input type="button" onclick="javascript: history.go(-1);" value="No" class="button2" />
		</div>
	</form>
</fieldset>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="forum_settings_page">

<form method="post" action="admin.php?act=forum_settings-s">
	<h1>Configuracion del Foro</h1><br />
	<p>
Cada ajuste que est� utilizando phpBB est� incluido en esta lista de opciones. Algunos de estos ajustes se
con discapacidad en el administrador CP phpBB real a los administradores de no permitir cambiar la configuraci�n de forma sensible.
		<fieldset>
			<legend>Configuracion del Foro</legend>
			<while id="forum_settings_whileloop">
				<dl>
					<dt><label for="<# CONFIG_NAME #>"><# CONFIG_NAME #>:</label></dt>
					<dd><input id="<# CONFIG_NAME #>" type="text" size="25" name="<# CONFIG_NAME #>" value="<# CONFIG_VALUE #>" /></dd>
				</dl>
			</endwhile>
		</fieldset>
		<input type="hidden" name="access_name" value="<# ACCESS_NAME #>">
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" value="Save Settings" />
		</fieldset>
	</p>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="delete_forum_page">

<fieldset>
	<legend>Forum Deletion</legend>
	<form action="admin.php?act=remove_forum-d" method="post">
		<h1>Confirm Deletion</h1><br />
		<p>
			Are you sure you wish to carry out this operation? 
			<br /><br />
			If you select "Yes" there is no undo.
		</p>
		<input type="hidden" name="access_name" value="<# ACCESS_NAME #>">
		<div style="text-align: center;">
			<input type="submit" value="Yes" class="button2" />&nbsp; 
			<input type="button" onclick="javascript: history.go(-1);" value="No" class="button2" />
		</div>
	</form>
</fieldset>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="edit_database_settings_page">

<form method="post" action="admin.php?act=database-e-s">
	<h1>Database Settings</h1>
	<fieldset>
		<legend>General Database Settings</legend>
		<dl>
			<dt><label for="sql_host">MySQL Host:</label><br /><span class="explain">If you are unsure of your MySQL host please contact your hosting company before continuing. Most commonly your MySQL host will be "localhost".</span></dt>
			<dd><input id="sql_host" type="text" size="25" name="sql_host" value="<# DATABASE_HOST #>" /></dd>
		</dl>
		<dl>
			<dt><label for="sql_database">MySQL Database Name:</label><br /><span class="explain">The entered database name must already exist in order to be used.</span></dt>
			<dd><input id="sql_database" type="text" size="25" name="sql_database" value="<# DATABASE_NAME #>" /></dd>
		</dl>
		<dl>
			<dt><label for="sql_username">MySQL Username:</label></dt>
			<dd><input id="sql_username" type="text" size="25" name="sql_username" value="<# DATABASE_USERNAME #>" /></dd>
		</dl>
		<dl>
			<dt><label for="sql_password">MySQL Password (optional):</label><br /><span class="explain">Even though it's optional, we don't recommend leaving this field empty.</span></dt>
			<dd><input id="sql_password" type="password" size="25" name="sql_password" value="<# DATABASE_PASSWORD #>" /></dd>
		</dl>
		<dl>
			<dt><label for="allow_signups">Allow Signups:</label><br /><span class="explain">On signup the database a forum will be stored in is determined randomly, and by setting this option to “No” will disable the ability for this database to be picked and used.</span></dt>
			<dd><input id="allow_signups" type="radio" size="25" name="allow_signups" value="1" <# ALLOW_SIGNUPS_YES #> /> Yes <input id="allow_signups" type="radio" size="25" name="allow_signups" value="0" <# ALLOW_SIGNUPS_NO #> /> No</dd>
		</dl>
	</fieldset>
	<input type="hidden" name="db_id" value="<# DATABASE_ID #>">
	<fieldset class="submit-buttons">
		<input class="button1" type="submit" value="Save" />
	</fieldset>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="new_database_page">

<form method="post" action="admin.php?act=database-n-s">
	<h1>New Database</h1><br />
	<p>
		Use the following form to add a new database to the available list of databases to store forums in. 
		By having multiple databases, not all the load of a ton of forums is placed on a single database, but 
		instead is distributed across many databases.
		<fieldset>
			<legend>General Database Settings</legend>
			<dl>
				<dt><label for="sql_host">MySQL Host:</label><br /><span class="explain">If you are unsure of your MySQL host please contact your hosting company before continuing. Most commonly your MySQL host will be "localhost".</span></dt>
				<dd><input id="sql_host" type="text" size="25" name="sql_host" value="localhost" /></dd>
			</dl>
			<dl>
				<dt><label for="sql_database">MySQL Database Name:</label><br /><span class="explain">The entered database name must already exist in order to be used.</span></dt>
				<dd><input id="sql_database" type="text" size="25" name="sql_database" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="sql_username">MySQL Username:</label></dt>
				<dd><input id="sql_username" type="text" size="25" name="sql_username" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="sql_password">MySQL Password (optional):</label><br /><span class="explain">Even though it's optional, we don't recommend leaving this field empty.</span></dt>
				<dd><input id="sql_password" type="password" size="25" name="sql_password" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="allow_signups">Allow Signups:</label><br /><span class="explain">On signup the database a forum will be stored in is determined randomly, and by setting this option to “No” will disable the ability for this database to be picked and used.</span></dt>
				<dd><input id="allow_signups" type="radio" size="25" name="allow_signups" value="1" /> Yes <input id="allow_signups" type="radio" size="25" name="allow_signups" value="0" checked="checked" /> No</dd>
			</dl>
		</fieldset>
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" value="Create Database" />
		</fieldset>
	</p>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="directory_manager_index_page">

<h1>Manage Categories</h1><br />
<p>
	Below is a complete list of categories that forums can be stored in to categorize them so that it is easier 
	for the average user to find forums related to their interests. Click on the category's name to view a list 
	of forums in it. If you delete a category that contains forums, then those forums will be set to the 
	“None – Private Forum” category which is not able to be deleted or edited. 
	<table cellspacing="1">
		<thead>
			<tr>
				<th>Category</th>
				<th>Forums In This Category</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<while id="directory_category_whileloop">
				<tr class="<# TRCLASS #>">
					<td><a target="_blank" href="index.php?act=directory-vc&cat=<# CATEGORY_ID #>" title="View Category"><# CATEGORY_NAME #></a></td>
					<td><# TOTAL_FORUMS #></td>
					<td><a href="admin.php?act=categories-e&cat=<# CATEGORY_ID #>">Edit</a> | <a href="admin.php?act=categories-r&cat=<# CATEGORY_ID #>">Delete</a></td>
				</tr>	
			</endwhile>
		</tbody>
	</table><br />
	<form method="post" action="admin.php?act=categories-n">
		<fieldset>
			<legend>Add Category</legend>
			<dl>
				<dt><label for="category_name">Category Name:</label><br /><span class="explain">Max Length of 255 characters.</span></dt>
				<dd><input id="category_name" type="text" size="25" maxlength="30" name="category_name" value="" /></dd>
			</dl>
		</fieldset>
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" value="Done" />
		</fieldset>
	</p>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="edit_directory_category_page">

<form method="post" action="admin.php?act=categories-e-s">
	<h1>Edit Category</h1>
	<fieldset>
		<legend>Category Settings</legend>
		<dl>
			<dt><label for="category_name">Category Name:</label><br /><span class="explain">Max Length of 255 characters.</span></dt>
			<dd><input id="category_name" type="text" size="25" name="category_name" value="<# CATEGORY_NAME #>" /></dd>
		</dl>
		<input type="hidden" name="category_id" value="<# CATEGORY_ID #>">
	</fieldset>
	<fieldset class="submit-buttons">
		<input class="button1" type="submit" value="Save Category" />
	</fieldset>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="delete_directory_category_page">

<fieldset>
	<legend>Category Deletion</legend>
	<form action="admin.php?act=categories-r-d" method="post">
		<h1>Confirm Deletion</h1><br />
		<p>
			Are you sure you wish to carry out this operation? 
			<br /><br />
			If you select "Yes" there is no undo.
		</p>
		<input type="hidden" name="category_id" value="<# CATEGORY_ID #>">
		<div style="text-align: center;">
			<input type="submit" value="Yes" class="button2" />&nbsp; 
			<input type="button" onclick="javascript: history.go(-1);" value="No" class="button2" />
		</div>
	</form>
</fieldset>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="administrator_manager_page">

<h1>Manage Administrators</h1><br />
<p>
	The Administrator Manager gives you the ability to add or remove people that can have access to this 
	control panel. By having this ability, no single administrative account has to be shared, and each 
	administrator can administer same as all the rest. The root administrator which usually has the ID of 
	1 cannot be deleted.
	<# PAGINATION_LINKS #>
	<table cellspacing="1">
		<thead>
			<tr>
				<th>#</th>
				<th>Username</th>
				<th>E-Mail Address</th>
				<th>IP Address</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<while id="administrator_listing_whileloop">
				<tr class="<# TRCLASS #>">
					<td><# ADMIN_ID #></td>
					<td><# USERNAME #></td>
					<td><# EMAIL_ADDRESS #></td>
					<td><# IP_ADDRESS #></td>
					<td><a href="admin.php?act=admins-ep&admin_id=<# ADMIN_ID #>">Edit Password</a> | <a href="admin.php?act=admins-r&admin_id=<# ADMIN_ID #>">Delete</a>
				</tr>
			</endwhile>
		</tbody>
	</table>
	<fieldset class="quick" style="float: left;">
		<a class="button2" href="admin.php?act=admins-n">Create New Account</a>
	</fieldset>
	
</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="delete_administrator_page">

<fieldset>
	<legend>Account Deletion</legend>
	<form action="admin.php?act=admins-r-d" method="post">
		<h1>Confirm Deletion</h1><br />
		<p>
			Are you sure you wish to carry out this operation? 
			<br /><br />
			If you select "Yes" there is no undo.
		</p>
		<input type="hidden" name="admin_id" value="<# ADMIN_ID #>">
		<div style="text-align: center;">
			<input type="submit" value="Yes" class="button2" />&nbsp; 
			<input type="button" onclick="javascript: history.go(-1);" value="No" class="button2" />
		</div>
	</form>
</fieldset>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="edit_administrator_page">

<h1>Edit Account Password</h1><br />
<form action="admin.php?act=admins-ep-s" method="post">
	<p>
		Use the following form to edit the password of the selected administrator.
		<fieldset>
			<legend>Account Password Settings</legend>
			<dl>
				<dt><label for="password">Current Administrator Password:</label><br /><span class="explain">The current password of this administrator must be entered correctly in order to do change.</span></dt>
				<dd><input id="password" type="password" size="25" maxlength="30" name="password" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="new_password">New Administrator Password:</label><br /><span class="explain">Must be between 6 and 30 characters in length. For more security we recommend that the password entered contains at least one numerical character.</span></dt>
				<dd><input id="new_password" type="password" size="25" maxlength="30" name="new_password" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="new_password-c">New Administrator Password (retype):</label></dt>
				<dd><input id="new_password-c" type="password" size="25" maxlength="30" name="new_password-c" value="" /></dd>
			</dl>
		</fieldset>
		<input type="hidden" name="admin_id" value="<# ADMIN_ID #>">
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" value="Change Password" />
		</fieldset>
	</p>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="new_administrator_page">

<h1>Creating An Account</h1><br />
<form action="admin.php?act=admins-n-s" method="post">
	<p>
		Use the following form to create a new administrator to help administer your forum hosting website.
		<fieldset>
			<legend>Administrator Account Settings</legend>
			<dl>
				<dt><label for="username">Administrator Username:</label><br /><span class="explain">Must be between 3 and 30 characters in length and only contain the characters: -_A-Za-z0-9</span></dt>
				<dd><input id="username" type="text" size="25" maxlength="30" name="username" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="password">Administrator Password:</label><br /><span class="explain">Must be between 6 and 30 characters in length. For more security we recommend that the password entered contains at least one numerical character.</span></dt>
				<dd><input id="password" type="password" size="25" maxlength="30" name="password" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="password-c">Administrator Password (retype):</label></dt>
				<dd><input id="password-c" type="password" size="25" maxlength="30" name="password-c" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="email_address">Administrator E-Mail Address:</label><br /><span class="explain">To be considered valid an email address can only contain the characters: -_A-Za-z0-9</span></dt>
				<dd><input id="email_address" type="text" size="25" name="email_address" value="" /></dd>
			</dl>
		</fieldset>
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" value="Save Settings" />
		</fieldset>
	</p>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
<template id="site_settings_page">

<form method="post" action="admin.php?act=site_settings-s">
	<h1>Site Settings</h1><br />
	<p>
		Shown below are some basic settings for your forum hosting website. If you wish to edit 
		the default configuration for your phpBB forums edit the file <b>/phpBB3/includes/install/defaults/default_config.php</b>. 
		More settings for your website will be available in the future.
		<fieldset>
			<legend>Site Settings</legend>
			<dl>
				<dt><label for="site_name">Website Name:</label></dt>
				<dd><input id="site_name" type="text" size="25" name="site_name" value="<# SITE_NAME #>" /></dd>
			</dl>
			<dl>
				<dt><label for="blocked_access_names">Disabled Access Names:</label><br /><span class="explain">This is a complete list of access names that are not allowed to be signed up. Separate each access names with a comma. Warning! Removing the 'mfh' access name from this list could potentially cause problems with your root database.</span></dt>
				<dd><input id="blocked_access_names" type="text" size="25" name="blocked_access_names" value="<# blocked_access_names #>" /></dd>
			</dl>
			<dl>
				<dt><label for="email_out">Incoming 'n Outgoing E-Mail Address:</label><br /><span class="explain">This is the email address that all emails will be sent from and to. To be considered valid an email address can only contain the characters: -_A-Za-z0-9</span></dt>
				<dd><input id="email_out" type="text" size="25" name="email_out" value="<# EMAIL_OUT #>" /></dd>
			</dl>
			<dl>
				<dt><label for="date_format">Date Format:</label><br /><span class="explain">For information on how to setup the date format go to <a href="http://php.net/date" target="_blank">php.net</a>.</span></dt>
				<dd><input id="date_format" type="text" size="25" name="date_format" value="<# DATE_FORMAT #>" /></dd>
			</dl>
			<dl>
				<dt><label for="max_results">Max Results:</label><br /><span class="explain">Max results of whatever to display on a single page.</span></dt>
				<dd>
					<select name="max_results" style="width: 200px;">
						<while id="max_results_forloop">
							<option value="<# MAX_RESULTS_SUM #>" <# MAX_RESULTS_SELECTED #>><# MAX_RESULTS_SUM #></option>
						</endwhile>
					</select>
				</dd>
			</dl>
		</fieldset>
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" value="Save Settings" />
		</fieldset>
	</p>
</form>

</template>
<!-- // -- |ACPT-BE| -- //  -->
