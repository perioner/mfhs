<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta http-equiv="imagetoolbar" content="no" />
<title>Panel de Administrador | MHFS</title>
<link href="./css/style.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="http://www.google-analytics.com/urchin.js"></script>
<script type="text/javascript" src="./source/includes/genjscript.js"></script>
</head>
<body class="ltr">
<div id="wrap">
	<div id="page-header">
		<h1>Panel de Control</h1>
		<p><a href="admin.php?">Panel de Administracion</a> &bull; <a href="index.php">Sitio Principal</a></p>
	</div>
	<div id="page-body">
	<div id="acp">
	<div class="panel">
		<span class="corners-top"><span></span></span>
			<div id="content">
				<div id="menu">
					<ul>
						<li><a href="admin.php?act=idx"><span>Inicio</span></a></li>
						<li><a href="admin.php?act=site_settings"><span>Configuracion</span></a></li>
						<li><a href="admin.php?act=database"><span>Base de Datos</span></a></li>
						<li><a href="admin.php?act=categories"><span>Directorio</span></a><li>
						<li><a href="admin.php?act=admins"><span>Administrador Manager</span></a></li>
						<if="$mfhclass->info->is_admin == true">
							<li><a href="admin.php?act=logout"><span>Logout Admin CP</span></a></li>
						</endif>
					</ul>
				</div>
			<div id="main">
