<!-- BEGIN: INDEX INTRO PAGE -->
<template id="index_intro_page">

<h1>Bienvenido a <# SITE_NAME #></h1><br />
<p>
	Bienvenido a <# SITE_NAME #>,
	<br /><br /> 	
phpBB es un software de foro de Internet elaborado por el Grupo <a href="http://www.phpbb.com"> phpBB </a>. escrito en
PHP y MySQL utilizando principalmente, es <a href="http://www.phpbb.com/about/features/compare.php"> comparable </a>
A otro software de otro foro.
	<br /><br /> 	

<# SITE_NAME #> es un servicio gratuito que ofrece el poder crear comunidad . Aunque es similar a un chat
Ambiente o a mensajes instaneos , <# SITE_NAME #> se pueden utilizar a largo plazo 
. Puede enviar un mensaje para que otros vean, incluso si no estan en linea en el momento. <# SITE_NAME #>
se puede utilizar para los amigos, clubes y empresas. <# SITE_NAME #> puede ser utilizado para hablar de su deporte favorito
o incluso un area de charla general donde la gente puede hablar lo que quieran. <# SITE_NAME #> proporciona el
mejor entorno posible para las comunidades en linea, pero tambien es facil de configurar y administrar.


	<br /><br />
	A que espera registre un nuevo Foro con nosotros .
	<br /><br />
	<fieldset class="submit-buttons">
		<a class="button1" href="index.php?act=signup">Continuar al Registro</a>
	</fieldset>
</p>

</template>
<!-- END: INDEX INTRO PAGE -->

<!-- BEGIN: PHPBB SIGNUP PAGE -->
<template id="phpbb_signup_page">

<div id="index_pb" style="text-align: center; display: none;">
	<p>
		Creando Foro
		<br /><br />
		<img src="css/images/progress_bar.gif" alt="Loading..." />
		<br /><br />
		Tu Foro esta en proceso de creacion
		<br /><br />
		<a href="javascript:void(0);" onclick="toggle('index_pb'); toggle('index_bpb');">Display Singup Form</a>
	</p>
</div>
<div id="index_bpb" style="display: block;">
	<form method="post" action="index.php?act=signup-p" id="signup_form">
		<h1>phpBB Forum Signup</h1><br />
		<p>
Rellene el siguiente formulario completo para crear su foro
<b>phpBB <# PACKAGED_PHPBB_VERSION #> </ b> para siempre. Una vez que su copia de
phpBB se establece se le presentara informacion sobre como acceder a ella.


			<fieldset>

				<legend>Configuracion General</legend>
				<dl>
					<dt><label for="access_name">Nombre de Acceso:</label><br /><span class="explain">El nombre de acceso que ingrese será usada para generar su propia URL personalizada foro. Un nombre de acceso debe ser entre 3 y 30 caracteres de longitud y sólo contienen los caracteres-_a-zA-Z0-9 </span></dt>
					<dd><input id="access_name" type="text" size="25" maxlength="30" name="access_name" value="" /></span></dd>
				</dl>
				<dl>
					<dt><label for="forum_name">Nombre del Foro (opcional):</label></dt>
					<dd><input id="forum_name" type="text" size="25" name="forum_name" value="" /></dd>
				</dl>
				<dl>
					<dt><label for="forum_category">Categoria del Foro:</label><br /><span class="explain"> categoría elegida determinará dónde colocar el foro dentro de nuestro directorio. Una vez registrado puede cambiar la categoría a través del administrador CP phpBB.</span></dt>
					<dd>
						<select name="forum_category" style="width: 190px;">
							<option value="">Selecionar Categoria</option>
							<option value="-1">Foro Privado</option>
							<while id="directory_categories_whileloop">
								<option value="<# CATEGORY_ID #>"><# CATEGORY_NAME #></option>
							</endwhile>
						</select>
					</dd>
				</dl>
			</fieldset>
			<fieldset>
			<legend>Configuracion de la Administracion</legend>
				<dl>
					<dt><label for="username">Usuario Admin:</label><br /><span class="explain">Debe estar entre 3 y 30 caracteres de longitud y sólo contienen los caracteres:-_A-Za-z0-9</span></dt>
					<dd><input id="username" type="text" size="25" maxlength="30" name="username" value="Admin" /></dd>
				</dl>
				<dl>
					<dt><label for="password">Contraseña :</label><br /><span class="explain">Debe tener entre 6 y 30 caracteres de longitud. Para mayor seguridad se recomienda que la contraseña introducida contenga al menos un carácter numérico.</span></dt>
					<dd><input id="password" type="password" size="25" maxlength="30" name="password" value="" /></dd>
				</dl>
				<dl>
					<dt><label for="password-c">Repetir Contraseña:</label></dt>
					<dd><input id="password-c" type="password" size="25" maxlength="30" name="password-c" value="" /></dd>
				</dl>
				<dl>
					<dt><label for="email_address">Direcion de Correo Electronico:</label><br /><span class="explain">Escribe un E-mail Valido </span></dt>
					<dd><input id="email_address" type="text" size="25" name="email_address" value="" /></dd>
				</dl>
			</fieldset>
			<div style="text-align: center;">
				<input type="checkbox" name="iagree" id="iagree" value="something"> <label for="iagree"> Antes de Darle a "Crear Foro" Tiene que acceptar nuestros <a href="index.php?act=rules">Terminos de Servicio</a>.</label>
			</div><br />
			<fieldset class="submit-buttons">
				<input class="button1" type="submit" id="submit" onclick="this.className = 'button1 disabled'; toggle('index_pb'); toggle('index_bpb'); document.upload_form.submit();" onsubmit="this.disabled = 'disabled';" name="submit" value="Crear Foro" />
			</fieldset>
		</p>
	</form>
</div>

</template>
<!-- END: PHPBB SIGNUP PAGE -->

<!-- BEGIN: FORUM CREATED PAGE -->
<template id="forum_created_page">

<h1>Forum Creado</h1><br />
<p>
	Felicitaciones, tu nuevo foro ha sido creado con éxito. A continuación se presentan algunos detalles relacionados con la
configuracion de tu Foro y su URL y cómo acceder a el.
	<fieldset>
		<legend>Informacion General del Foro</legend>
		<dl>
			<dt><label>URL del Foro:</label></dt>
			<dd id="forum_url"><a href="<# BASE_URL #>forums/<# ACCESS_NAME #>/"><# BASE_URL #>forums/<# ACCESS_NAME #>/</a></dd>
		</dl>
		<dl>
			<dt><label for="acp_url">Direcion del Panel de Admin:</label></dt>
			<dd id="acp_url"><a href="<# BASE_URL #>forums/<# ACCESS_NAME #>/adm/"><# BASE_URL #>forums/<# ACCESS_NAME #>/adm/</a></dd>
		</dl>
		<dl>
			<dt><label for="forum_name">Nombre de Foro:</label></dt>
			<dd id="forum_name"><# FORUM_NAME #></dd>
		</dl>
	</fieldset>
	<fieldset>
		<legend>Configuracion del Administrador</legend>
		<dl>
			<dt><label for="username">Usuario:</label></dt>
			<dd id="username"><# ADMIN_USERNAME #></dd>
		</dl>
		<dl>
			<dt><label for="password">Contraseña :</label></dt>
			<dd id="password"><# ADMIN_PASSWORD #></dd>
		</dl>
		<dl>
			<dt><label for="email_address">E-mail:</label></dt>
			<dd id="email_address"><# ADMIN_EMAIL_ADDRESS #></dd>
		</dl>
	</fieldset>
	<fieldset class="submit-buttons">
		<a target="_blank" class="button1" href="<# BASE_URL #>forums/<# ACCESS_NAME #>/">Continuar al Foro</a>
	</fieldset>
</p>

</template>
<!-- END: FORUM CREATED PAGE -->

<!-- BEGIN: TERMS OF SERVICE PAGE -->
<template id="terms_of_service_page">

<h1>Terminos de Servicio</h1><br />
<p>
	<i>Ultima modificaicon de los Terminos de Servicio<# MODIFICATION_TIME #></i>
	<br /><br />
	Al usar este sitio o la creación de un foro, usted acepta que quedará vinculado por estos términos y condiciones y todos los cambios
en el tal como fue anunciado.
	<br /><br />
	Usted debe tener 13 o más para usar este sitio, crear una cuenta, visitar cualquiera de nuestros foros, o la creación de un foro.
	<br /><br />
<# SITE_NAME #> se proporciona tal como es, sin garantías de aptitud para el uso particular. Usted se compromete a
<# SITE_NAME #> Tiene limitaciones, incluyendo todos los cambios en estos términos como hechas y publicadas en este sitio, con
o sin notificación previa.
	<br /><br />
El contenido de un foro es la publicaciones, mensajes, enlaces, correos electrónicos, archivos subidos, o cualquier otro dato de usuario creadas
("Content"). El cartel es responsable de cualquier Contenido transmitido a <# SITE_NAME #> servidor. A medida que el
creador de un foro, es su trabajo para asegurarse de que el contenido publicado en el foro cumple con las normas y
estas Condiciones de servicio. <# SITE_NAME #> no garantiza la veracidad, legitimidad, calidad o integridad de la
de cualquier Contenido. <# SITE_NAME #> no garantiza que el contenido no será perdido, borrado o dañado.
	<br /><br />
	A continuación se muestra una lista de temas prohibidos que no está permitido <br />
	<ol style="list-style: upper-roman inside;">
		<li>Contenido que viole cualquier ley, incluso, las leyes de cualquier localidad, estado, país, nación o el derecho internacional. </li>
		<li>Contenido que infrinja los derechos (de autor, patentes y derechos de privacidad). </li>
		<li>Contenido que sea amenazante, difamatorio, racista, abusivo, obsceno o profano</li> 
		<li> Contenido que lleva, virus o cualquier otro software informáticos dañinos. </ li>
		<li> contenido que es falso o injuriantes. </ li>
<li> Contenido que implica esquemas de pirámide o permite el spam. </ li>
<li> contenido que promueva o implica el juego, las drogas ilegales o el terrorismo. </ li>
<li> contenido que promueva o realice Warez, claves de CD, números de serie, etc
<li> Contenido que consiste en la suplantación de cualquier otra persona o entidad.
<li> Contenido que incluye la pornografía, desnudos o contenido sexual de ningún tipo. </ li>
	</ol>
	Usted no puede publicar, cargar, enlazar, o por correo electrónico cualquier contenido que contenga, promueve, enseña acerca de, o proporciona
Contenido prohibido. Como el creador de un foro, usted es responsable de asegurarse de que ningún contenido prohibido
existA en el foro y que ninguna parte de tu foro viola los Términos de Servicio o las Condiciones de uso.
	<br /><br />
	<# SITE_NAME #> se reserva el derecho de verificar cualquier foro para comprobar que no contenga
contenido prohibido. <# SITE_NAME #> tiene derecho a acceder a cualquier foro y su contenido relacionado y eliminar,
modificar o restringir el acceso a cualquier contenido. Usted acepta que <# SITE_NAME #> pueda divulgar y utilizar cualquier contenido
en cualquier momento.
<br /> <br />
Cualquier Contenido publicado en <# SITE_NAME #> es de la exclusiva responsabilidad de la persona que lo publicó.
<# SITE_NAME #> no es responsable de controlar y no controla ni aprueba Contenido en cualquier momento. Usted se hará cargo
cualquier riesgo de confiar en la exactitud, validez o legitimidad de Contenido que usted publique.
<br /> <br />
<# SITE_NAME #> tiene el derecho de publicar, almacenar y mantener el contenido. Todo el contenido cargado o enviado a
<# SITE_NAME #> pueden ser publicados por <# SITE_NAME #>.
<br /> <br />
Usted acepta que <# SITE_NAME #> podrá eliminar su cuenta en cualquier momento por cualquier razón y puede borrar de su foro,
o de su cuenta en cualquier foro <# SITE_NAME #>. <# # SITE_NAME> puede prohibir su dirección de correo electrónico, nombre de usuario, etc o
eliminar / modificar cualquier contenido de cualquier foro o de su cuenta, sin preaviso.
<br /> <br />
Su cuenta o foro puede ser eliminado por cualquier violación de laS Normas, Términos de servicio, o
por cualquier otra razón de <# SITE_NAME #> . <# SITE_NAME #> no está obligado a explicar por qué su cuenta / foro / contenido se ha eliminado
o editado.
<br /> <br />
<# SITE_NAME #> no garantiza que el servicio es seguro, libre de errores o interrupciones <# SITE_NAME #>
<br /> <br />
<br /> <br />
Usted entiende y acepta que  <# SITE_NAME #> no se hace responsable de los daños y perjuicios, directos o indirectos,
como resultado del uso y / o la incapacidad para utilizar el Servicio.
<br /> <br />
<br /> <br />
Usted es el único responsable de sus interacciones con otros usuarios del Servicio. <# # SITE_NAME> reserva
el derecho, pero no está obligado, para mediar o resolver las controversias entre usted y otros usuarios del Servicio.
<br /> <br />
<br /> <br />
Usted se compromete a no vender el servicio o el acceso al Servicio, que no sean de contenido que se crea.
<br /> <br />
<br /> <br />
Si usted nota un <# SITE_NAME #> miembro de romper alguna de las reglas, por favor, avísenos haciendo clic en
<a href="index.php?act=contact_us"> aquí </ a> </ p>

</template>
<!-- END: TERMS OF SERVICE PAGE -->

<!-- BEGIN: FORUM DIRECTORY INDEX PAGE -->
<template id="directory_index_page">

<h1>Forum Directory</h1><br />
<table cellspacing="1">
	<thead>
		<tr>
			<th>Category</th>
			<th>Forums In This Category</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<while id="directory_category_whileloop">
			<tr class="<# TRCLASS #>">
				<td><# CATEGORY_NAME #></td>
				<td><# TOTAL_FORUMS #></td>
				<td><a href="index.php?act=directory-vc&cat=<# CATEGORY_ID #>">View Category</a></td>
			</tr>
		</endwhile>
	</tbody>
</table>		

</template>
<!-- END: FORUM DIRECTORY INDEX PAGE -->

<!-- BEGIN: FORUM DIRECTORY CATEGORY INDEX PAGE -->
<template id="directory_view_category_page">

<a href="index.php?act=directory" style="float: right;">&laquo; Back</a>
<h1>Viewing Category</h1>
<p>
	<# PAGINATION_LINKS #>
	<table cellspacing="1">
		<thead>
			<tr>
				<th>Nombre del For</th>
				<th>Total Clics</th>
				<th>Dato de Creacion</th>
				<th>Total Usuarios</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<while id="category_listing_whileloop">
				<tr class="<# TRCLASS #>">
					<td><a href="<# BASE_URL #>forums/<# ACCESS_NAME #>/?r=<# TOTAL_HITS #>"><# FORUM_NAME #></a></td>
					<td><# HUMAN_TOTAL_HITS #></td>
					<td><# DATE_CREATED #></td>
					<td><# TOTAL_MEMBERS #></td>
					<td><a href="<# BASE_URL #>forums/<# ACCESS_NAME #>/?r=<# TOTAL_HITS #>">View Forum</a></td>
				</tr>
			</endwhile>
		</tbody>
	</table>
</p>

</template>
<!-- END: FORUM DIRECTORY CATEGORY INDEX PAGE -->

<!-- BEGIN: CONTACT US PAGE -->
<template id="contact_us_page">

<form method="post" action="index.php?act=contact_us-s" id="signup_form">
	<h1>Contactar</h1><br />
	<p>
		<p> Contacta con los Administradores de Soporte Foros
		<fieldset>
			<legend>Mensaje</legend>
			<dl>
				<dt><label for="full_name">Nombre:</label></dt>
				<dd><input id="full_name" type="text" size="25" maxlength="30" name="full_name" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="email_address">E-Mail Address:</label><br /><span class="explain">Tiene que ser un E-Mail valido</span></dt>
				<dd><input id="email_address" type="text" size="25" name="email_address" value="" /></dd>
			</dl>
			<dl>
				<dt><label for="message_body">Mensajee:</label><br /><span class="explain">Intenta ser preciso y indicarnos lo que quieres o el problema que tienes</span></dt>
				<dd><textarea id="message_body" name="message_body" rows="10" cols="60"></textarea></dd>
			</dl>
		</fieldset>
		<fieldset class="submit-buttons">
			<input class="button1" type="submit" id="submit" name="submit" value="Enviar" />&nbsp;
		</fieldset>
	</p>
</form>

</template>
<!-- END: CONTACT US PAGE -->

<!-- BEGIN: CONTACT US E-MAIL -->
<template id="contact_us_email">
El Email ha sido recibido en  <# SITE_NAME #>

----------------------------------

<# FULL_NAME #> <<# EMAIL_ADDRESS #>> wrote,

<# EMAIL_BODY #>
</template>
<!-- END: CONTACT US E-MAIL -->
