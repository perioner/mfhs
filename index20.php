<?php
	// ======================================== \
	// Package: Mihalism Multi Forum Host 
	// Version: 3.0.0
	// Copyright (c) 2007, 2008 Mihalism, Inc.
	// License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GNU Public License
	// ======================================== /
	
require_once "./source/includes/data.php";

	switch ($mfhclass->input->get_vars['act']) {
		case "signup":
			$mfhclass->templ->page_title = "{$mfhclass->info->config['site_name']} &raquo; Signup"; 
	       		$mfhclass->templ->html = "\t\t\t<div id=\"index_pb\" style=\"text-align: center; display: none;\">
			<p>Creating forum...<br /><br />
			<img src=\"css/images/progress_bar.gif\" alt=\"Loading...\" /> <br /><br />
			Your fourm is in the process of being created.<br /><br />
			<a href=\"javascript:void(0);\" onclick=\"toggle('index_pb'); toggle('index_bpb');\">Display Singup Form</a></p>
			</div>
			<div id=\"index_bpb\" style=\"display: block;\">
			<form method=\"post\" action=\"index.php?act=signup-p\" id=\"signup_form\">
			<h1>phpBB Forum Signup</h1><br />
			<p>Fill in the following form completely to set up your very own free copy 
			<b>phpBB {$mfhclass->info->phpbb_version}</b> to keep forever. Once your copy of 
			phpBB is set up you will be presented with information on how to access it.
			<fieldset>
			<legend>Forum Settings</legend>
			<dl>
			<dt><label for=\"access_name\">Access Name:</label><br /><span class=\"explain\">The access name that you enter will be used to generate your own custom forum URL. An access name must be between 3 and 30 characters in length and only contain the characters -_a-zA-Z0-9</span></dt>
			<dd><input id=\"access_name\" type=\"text\" size=\"25\" maxlength=\"30\" name=\"access_name\" value=\"\" /></span></dd>
			</dl>
			<dl>
			<dt><label for=\"forum_name\">Forum Name (optional):</label></dt>
			<dd><input id=\"forum_name\" type=\"text\" size=\"25\" name=\"forum_name\" value=\"\" /></dd>
			</dl>
			<dl>
			<dt><label for=\"forum_category\">Forum Category:</label><br /><span class=\"explain\">The category you chose will determine where to place your forum within our directory. Once signed up you can change the category via the phpBB Admin CP.</span></dt>
			<dd><select name=\"forum_category\" style=\"width: 190px;\">
			<option value=\"\">Select Category</option>
			<option value=\"-1\">None - Private Forum</option>";
			$sql = $mfhclass->db->query("SELECT * FROM `mfh_directory_categories` ORDER BY `category_name` ASC;");
			while ($row = $mfhclass->db->fetch_array($sql)) {
				$mfhclass->templ->html .= "<option value=\"{$row['category_id']}\">{$row['category_name']}</option>\n";
			}
			$mfhclass->templ->html .= "</select></dd>
			</dl>
			</fieldset>
			<fieldset>
			<legend>Root Administrator Settings</legend>
			<dl>
			<dt><label for=\"username\">Administrator Username:</label><br /><span class=\"explain\">Must be between 3 and 30 characters in length and only contain the characters: -_A-Za-z0-9</span></dt>
			<dd><input id=\"username\" type=\"text\" size=\"25\" maxlength=\"30\" name=\"username\" value=\"Admin\" /></dd>
			</dl>
			<dl>
			<dt><label for=\"password\">Administrator Password:</label><br /><span class=\"explain\">Must be between 6 and 30 characters in lenght. For more security we recommend that the password entered contains at least one numerical character.</span></dt>
			<dd><input id=\"password\" type=\"password\" size=\"25\" maxlength=\"30\" name=\"password\" value=\"\" /></dd>
			</dl>
			<dl>
			<dt><label for=\"password-c\">Administrator Password (confirm):</label></dt>
			<dd><input id=\"password-c\" type=\"password\" size=\"25\" maxlength=\"30\" name=\"password-c\" value=\"\" /></dd>
			</dl>
			<dl>
			<dt><label for=\"email_address\">Administrator E-Mail Address:</label><br /><span class=\"explain\">To be considered valid an email address can only contain the characters: -_A-Za-z0-9</span></dt>
			<dd><input id=\"email_address\" type=\"text\" size=\"25\" name=\"email_address\" value=\"\" /></dd>
			</dl>
			</fieldset>
			<div align=\"center\">
			<input type=\"checkbox\" name=\"iagree\" id=\"iagree\" value=\"something\"> <label for=\"iagree\"> By clicking \"Finish Signup\" I agree to the <a href=\"index.php?act=rules\">Terms of Service</a>.</label>
			</div><br />
			<fieldset class=\"submit-buttons\">
			<input class=\"button1\" type=\"submit\" id=\"submit\" onclick=\"this.className = 'button1 disabled'; toggle('index_pb'); toggle('index_bpb'); document.upload_form.submit();\" onsubmit=\"this.disabled = 'disabled';\" name=\"submit\" value=\"Finish Signup\" />
			</fieldset></p>
			</form></div>";
			break;
		case "signup-p":
			$mfhclass->templ->page_title = "{$mfhclass->info->config['site_name']} &raquo; Singup &raquo; Singup Complete";
			$mfhclass->input->post_vars['access_name'] = preg_replace("/-/", "_", strtolower($mfhclass->input->post_vars['access_name']));

			if ($mfhclass->input->post_vars['iagree'] == NULL || $mfhclass->input->post_vars['access_name'] == NULL || $mfhclass->input->post_vars['username'] == NULL || $mfhclass->input->post_vars['password'] == NULL || $mfhclass->input->post_vars['password-c'] == NULL || $mfhclass->input->post_vars['email_address'] == NULL || $mfhclass->input->post_vars['forum_category'] == NULL) {
				$mfhclass->templ->error("Please ensure that all required fields of the singup form has been filled in.");
			} elseif (!$mfhclass->funcs->valid_email($mfhclass->input->post_vars['email_address'])) {
				$mfhclass->templ->error("Please ensure that the administrator email address entered is valid.");
			} elseif ($mfhclass->input->post_vars['password'] != $mfhclass->input->post_vars['password-c']) {
				$mfhclass->templ->error("Please ensure that the administrator passwords you have entered exactly match each other. ");
			} elseif (strlen($mfhclass->input->post_vars['password']) < 6 || strlen($mfhclass->input->post_vars['password']) > 30) {
				$mfhclass->templ->error("Please ensure you have entered a valid administrator password.");
			} elseif (!$mfhclass->funcs->valid_string($mfhclass->input->post_vars['username']) || strlen($mfhclass->input->post_vars['username']) < 3 || strlen($mfhclass->input->post_vars['username']) > 30) {
				$mfhclass->templ->error("Please ensure you have entered a valid administrator username.");
			} elseif (!$mfhclass->funcs->valid_string($mfhclass->input->post_vars['access_name']) || strlen($mfhclass->input->post_vars['access_name']) < 3 || strlen($mfhclass->input->post_vars['access_name']) > 30) {
				$mfhclass->templ->error("Please ensure you have entered a valid access name");
			} elseif ($mfhclass->funcs->forum_exists($mfhclass->input->post_vars['access_name']) || in_array($mfhclass->input->post_vars['access_name'], array("access_name", "help", "support", "mfh"))) {
				$mfhclass->templ->error("Sorry but the requested access name is already in use.");
			} elseif ($mfhclass->db->total_rows($mfhclass->db->query("SELECT * FROM `mfh_forum_databases` WHERE `allow_signups` = 1 ORDER BY RAND() LIMIT 1;")) < 1) {
				$mfhclass->templ->error("Sorry but signups are disabled.");
			} else {		
				$database_info = $mfhclass->db->fetch_array($mfhclass->db->query("SELECT * FROM `mfh_forum_databases` WHERE `allow_signups` = 1 ORDER BY RAND() LIMIT 1;"));

				$replacevalues = array(	"DBNAME" => $mfhclass->input->post_vars['access_name'],
							"EMAIL" => $mfhclass->input->post_vars['email_address'],
							"SERVER_NAME" => $mfhclass->info->site_url,
							"SERVER_PROTOCOL" => "http://",
							"SITE_NAME" => $mfhclass->input->post_vars['forum_name'],
							"UPLOAD_PATH" => $mfhclass->input->post_vars['access_name'],
							"USERNAME" => $mfhclass->input->post_vars['username'],
							"USERNAME_MINI" => strtolower($mfhclass->input->post_vars['username']),
							"PASSWORD" => $mfhclass->passwordfuncs->HashPassword($mfhclass->input->post_vars['password']));

				$myFile = "{$mfhclass->info->root_path}/sql/phpbb-{$mfhclass->info->phpbb_version}.sql";
				$fh = fopen($myFile, 'r');
				$sqlscript = fread($fh, filesize($myFile));
				fclose($fh);

				$sqlscript = str_replace("##DBNAME##", $replacevalues["DBNAME"], $sqlscript);
				$sqlscript = str_replace("##EMAIL##", $replacevalues["EMAIL"], $sqlscript);
				$sqlscript = str_replace("##SERVER_NAME##", $replacevalues["SERVER_NAME"], $sqlscript);
				$sqlscript = str_replace("##SERVER_PROTOCOL##", $replacevalues["SERVER_PROTOCOL"], $sqlscript);
				$sqlscript = str_replace("##SITE_NAME##", $replacevalues["SITE_NAME"], $sqlscript);
				$sqlscript = str_replace("##UPLOAD_PATH##", $replacevalues["UPLOAD_PATH"], $sqlscript);
				$sqlscript = str_replace("##USERNAME##", $replacevalues["USERNAME"], $sqlscript);
				$sqlscript = str_replace("##USERNAME_MINI##", $replacevalues["USERNAME_MINI"], $sqlscript);
				$sqlscript = str_replace("##PASSWORD##", $replacevalues["PASSWORD"], $sqlscript);

				$sqlarray = explode("(EOL)", $sqlscript);

				for ($i = 0; $i < count($sqlarray) - 1; $i++) {
					$mfhclass->db->query($sqlarray[$i], $database_info['database_id']);
				}

				if (!mkdir("{$mfhclass->info->root_path}phpBB3/files/{$mfhclass->input->post_vars['access_name']}/", 0777)) {
					$mfhclass->templ->error("Failed to create upload folder <b>{$mfhclass->info->root_path}phpBB3/files/{$mfhclass->input->post_vars['access_name']}/</b>.");
				}

				$mfhclass->db->query("INSERT INTO `mfh_hosted_forums` (`database_id`, `access_name`, `time_started`, `total_hits`, `category_id`, `ip_address`, `email_address`) VALUES ('{$database_info['database_id']}', '{$mfhclass->input->post_vars['access_name']}', ".time().", 1, {$mfhclass->input->post_vars['forum_category']}, '{$mfhclass->input->server_vars['ip_address']}', '{$mfhclass->input->post_vars['email_address']}');");
				
				$mfhclass->templ->html = "<h1>Forum Created</h1><br />
				<p>Congratulations, your new forum has been successfully created. Below are some details related to 
				your new phpBB powered forum and how to access it.
				<fieldset>
				<legend>Forum Information</legend>
				<dl>
				<dt><label for=\"forum_url\">Forum URL:</label></dt>
				<dd id=\"forum_url\"><a href=\"{$mfhclass->info->site_url}forums/{$mfhclass->input->post_vars['access_name']}/\">{$mfhclass->info->site_url}forums/{$mfhclass->input->post_vars['access_name']}/</a></dd>
				</dl>
				<dl>
				<dt><label for=\"acp_url\">Admin Control Panel URL:</label></dt>
				<dd id=\"acp_url\"><a href=\"{$mfhclass->info->site_url}forums/{$mfhclass->input->post_vars['access_name']}/adm/\">{$mfhclass->info->site_url}forums/{$mfhclass->input->post_vars['access_name']}/adm/</a></dd>
				</dl>
				<dl>
				<dt><label for=\"forum_name\">Forum Name:</label></dt>
				<dd id=\"forum_name\">".(($mfhclass->input->post_vars['forum_name'] == NULL) ? "<i>No Information</i>" : $mfhclass->input->post_vars['forum_name'])."</dd>
				</dl>
				</fieldset>
				<fieldset>
				<legend>Administrator Information</legend>
				<dl>
				<dt><label for=\"username\">Administrator Username:</label></dt>
				<dd id=\"username\">{$mfhclass->input->post_vars['username']}</dd>
				</dl>
				<dl>
				<dt><label for=\"password\">Administrator Password:</label></dt>
				<dd id=\"password\">{$mfhclass->input->post_vars['password']}</dd>
				</dl>
				<dl>
				<dt><label for=\"email_address\">Administrator E-Mail Address:</label></dt>
				<dd id=\"email_address\">{$mfhclass->input->post_vars['email_address']}</dd>
				</dl>
				</fieldset>
				<fieldset class=\"submit-buttons\">
				<a target=\"_blank\" class=\"button1\" href=\"{$mfhclass->info->site_url}forums/{$mfhclass->input->post_vars['access_name']}/\">Continue to Forum</a>
				</fieldset></p>";
			}
			break;
			
		case "rules":
			$mfhclass->templ->page_title = "{$mfhclass->info->config['site_name']} &raquo; Terms of Service";

			$mfhclass->templ->templ_vars[] = array(
				"MODIFICATION_TIME" => date($mfhclass->info->config['date_format'], filemtime("{$mfhclass->info->root_path}index.php")),
				"SITE_NAME"         => $mfhclass->info->config['site_name'],
				"EMAIL_OUT"         => $mfhclass->info->config['email_out'],
			); 
		
			$mfhclass->templ->output("home", "terms_of_service_page");
			break;
		case "directory":
			$mfhclass->templ->page_title = "{$mfhclass->info->config['site_name']} &raquo; Forum Directory";

			$sql = $mfhclass->db->query("SELECT * FROM `mfh_directory_categories` ORDER BY `category_name` ASC;");
			while ($row = $mfhclass->db->fetch_array($sql)) {
				$mfhclass->templ->templ_globals['get_whileloop'] = true;
			
				$mfhclass->templ->templ_vars[] = array(
					"TRCLASS"       => $trclass = (($trclass == "row1") ? "row2" : "row1"),
					"CATEGORY_ID"   => $row['category_id'],
					"CATEGORY_NAME" => $row['category_name'],
					"TOTAL_FORUMS"  => $mfhclass->funcs->format_number($mfhclass->db->total_rows($mfhclass->db->query("SELECT * FROM `mfh_hosted_forums` WHERE `category_id` = '{$row['category_id']}';"))),
				);
			
				$mfhclass->templ->templ_globals['directory_category_whileloop'] .= $mfhclass->templ->parse_template("home", "directory_index_page");
			
				unset($mfhclass->templ->templ_vars, $mfhclass->templ->templ_globals['get_whileloop']);
			}
			
			$mfhclass->templ->output("home", "directory_index_page");
			break;
		case "directory-vc":
			$mfhclass->templ->page_title = "{$mfhclass->info->config['site_name']} &raquo; Forum Directory &raquo; Viewing Category";

			$sql = $mfhclass->db->query("SELECT * FROM `mfh_hosted_forums` WHERE `category_id` = '{$mfhclass->input->get_vars['cat']}' ORDER BY `total_hits` DESC LIMIT <# QUERY_LIMIT #>;");
			if ($mfhclass->db->total_rows($sql) < 1 || $mfhclass->input->get_vars['cat'] == -1) {
				$mfhclass->templ->error("Category is empty or doesn't exist.", true);
			} else {
				while ($row = $mfhclass->db->fetch_array($sql)) {
					$forum_name = $mfhclass->db->fetch_array($mfhclass->db->query("SELECT * FROM `{$row['access_name']}_config` WHERE `config_name` = 'sitename';", $row['database_id']));

					$mfhclass->templ->templ_globals['get_whileloop'] = true;
			
					$mfhclass->templ->templ_vars[] = array(
						"TRCLASS"            => $trclass = (($trclass == "row1") ? "row2" : "row1"),
						"FORUM_NAME"         => $forum_name['config_value'],
						"TOTAL_HITS"         => $row['total_hits'],
						"HUMAN_TOTAL_HITS"   => $mfhclass->funcs->format_number($row['total_hits']),
						"ACCESS_NAME"        => $row['access_name'],
						"BASE_URL"           => $mfhclass->info->base_url,
						"DATE_CREATED"       => date($mfhclass->info->config['date_format'], $row['time_started']),
						"TOTAL_MEMBERS"      => $mfhclass->funcs->format_number(($mfhclass->db->total_rows($mfhclass->db->query("SELECT * FROM `{$row['access_name']}_users`;", $row['database_id'])) - $mfhclass->db->total_rows($mfhclass->db->query("SELECT * FROM `{$row['access_name']}_bots`;", $row['database_id']))) - 1),
					);
			
					$mfhclass->templ->templ_globals['category_listing_whileloop'] .= $mfhclass->templ->parse_template("home", "directory_view_category_page");
			
					unset($mfhclass->templ->templ_vars, $mfhclass->templ->templ_globals['get_whileloop']);
				}
			}
			
			$mfhclass->templ->templ_vars[] = array(
				"PAGINATION_LINKS"   => $mfhclass->templ->pagelinks("index.php?act=directory-vc&cat={$mfhclass->input->get_vars['cat']}", $mfhclass->db->total_rows($mfhclass->db->query("SELECT * FROM `mfh_hosted_forums` WHERE `category_id` = '{$mfhclass->input->get_vars['cat']}' ORDER BY `total_hits` DESC;"))),
			);
			
			$mfhclass->templ->output("home", "directory_view_category_page");
			break;
		case "contact_us":
			$mfhclass->templ->page_title = "{$mfhclass->info->config['site_name']} &raquo; Contact {$mfhclass->info->config['site_name']}";
			
			$mfhclass->templ->templ_vars[] = array(
				"SITE_NAME" => $mfhclass->info->config['site_name'],
			); 
		
			$mfhclass->templ->output("home", "contact_us_page");
		case "contact_us-s":
			$mfhclass->templ->page_title = "{$mfhclass->info->config['site_name']} &raquo; Contact {$mfhclass->info->config['site_name']}";
			
			if ($mfhclass->funcs->is_null($mfhclass->input->post_vars['email_address']) == true || $mfhclass->funcs->is_null($mfhclass->input->post_vars['message_body']) == true || $mfhclass->funcs->is_null($mfhclass->input->post_vars['full_name']) == true) {
				$mfhclass->templ->error("Please ensure that all required fields of the form on the previous page had been filled in correctly.", true);
			} elseif ($mfhclass->funcs->valid_email($mfhclass->input->post_vars['email_address']) == false) {
				$mfhclass->templ->error("Please ensure that the email address you entered is valid.", true);
			} else {
				$mfhclass->templ->templ_vars[] = array(
					"SITE_NAME"     => $mfhclass->info->config['site_name'],
					"FULL_NAME"     => $mfhclass->input->post_vars['full_name'],
					"EMAIL_ADDRESS" => $mfhclass->input->post_vars['email_address'],
					"EMAIL_BODY"    => strip_tags(str_replace("<br />", "\n", $mfhclass->input->post_vars['message_body'])),
				);
				
				$message_body = $mfhclass->templ->parse_template("home", "contact_us_email"); 
	
				if (mail($mfhclass->info->config['email_out'], "Site Contact ({$mfhclass->info->config['site_name']})", $message_body, "From: {$mfhclass->info->config['site_name']} <{$mfhclass->info->config['email_out']}>") == true) {
					$mfhclass->templ->success("The {$mfhclass->info->config['site_name']} team has been successfully contacted. <br /><br /> <a href=\"index.php\">Site Index</a>", true);
				} else {
					$mfhclass->templ->error("Failed to send email due to an unknown problem.", true);
				}
			}
		default:
			$mfhclass->templ->templ_vars[] = array(
				"SITE_NAME" => $mfhclass->info->config['site_name'],
			); 
		
			$mfhclass->templ->output("home", "index_intro_page");
	}

	$mfhclass->templ->output();	

?>
